Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.

Release info for c2py:
======================================================================

0.0.1rc7 (2019-04-13):

  * Bugfixes:

    * Integer division now works correctly; i.e. `//' is no longer
      ignored

    * An empty `global' statement (i.e. `global;') no longer causes a
      segmentation fault

    * Don't end a do-while loop if an empty line is inputed after the
      closing `}' but before the `while' in interactive mode

    * do-while loops now work in interactive mode

  * Features:

    * Allow changing of output indent size at run time with `-I' or
      `--indent-size'

    * Add the `raise' statement for raising exceptions

    * Support `in' constructs in expressions such as `i in 1, 2, 3;'

    * Enable tracing support with `-d', `--debug', `-t', or `--trace'
      when compiled with `--enable-trace' passed to `configure'

  * Changes:

    * Change `-I' or `--indent-size' to `-p' or `--indent-prompt'
      since `-I' and `--indent-size' are now being used to indent the
      output

    * `del', `return', and `global' are now considered expressions
      (which allows them to be used in single line conditionals and
      stuff like that)

    * Allow variable declarations to use types within other objects
      such as `foo.bar baz;'

    * Allow python style for loops without parentheses after the `in'
      like this: `for (i in 1, 2, 3) print(i);'

    * Don't require GNU Bison at compile time

    * Support Python versions >= 3.3 rather than just versions >= 3.6

0.0.1rc6 (2019-04-07):

  * Bugfixes:
  
    * Fixed escaped characters so that \\<char> does not escape <char>
      (where <char> is a single character)

    * Fixed nested do loops where the last \n would be overwritten by
      the `del __c2py_do_*' statement

  * Features:

    * Primary and secondary prompts in interactive mode now use
      `sys.ps1' and `sys.ps2' if available when in shebang mode and
      compiled with libpython3

  * Changes:

    * Help message now outputs Savannah bug tracker URL and the bug
      mailing list address

    * Prompt uses secondary prompt when inputing triple quoted string
      literals across multiple lines in interactive mode (although
      inputing *single* quoted string literals across multiple lines
      still uses the primary prompt when not inside a block)


0.0.1rc5 (2019-03-30):

  * Initial release
