/************************************************************************\
 * C2py converts cpy code (with a C-like syntax) to Python code.        *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                 *
 *                                                                      *
 * This file is part of c2py.                                           *
 *                                                                      *
 * C2py is free software: you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * C2py is distributed in the hope that it will be useful,              *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with c2py.  If not, see <https://www.gnu.org/licenses/>.       *
\************************************************************************/

/* input.c -- getc() and ungetc() frontend to get characters from
   `command_string' or interface with GNU Readline. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <string.h>
#include <stdlib.h>

#include "readline.h"
#include "c2py.h"
#include "input.h"

/* When we should use sys.ps1 and when we should use sys.ps2 */
#define PS1_COND (!(PS2_COND)
#define PS2_COND (wait_string || use_ps2)

/* Default prompts

   FIXME: When Python is run in interactive mode, sys.ps1 and sys.ps2
   are set. When c2py is run in interactive mode they are not. This is
   why we need the default prompts. Figure out how Python sets sys.ps*
   when in interactive mode. */
#define PS1	">>> "
#define PS2	"... "
#define PROMPT	(PS2_COND ? PS2 : PS1)

/* The python variable (under `sys') that we should use to get the
   prompt if available */
#define PROMPT_VAR (PS2_COND ? "ps2" : "ps1")

/* A list of ungotten characters */
struct unget_list {
  struct unget_list *prev;
  char c;
};

static unsigned int input_string_index;
static char *input_string = NULL;
static unsigned int command_string_index = 0;
static char unget_buf[8];
static unsigned char unget_buf_index = 0;
static struct unget_list *unget_list = NULL;

int get_input(void) {
  int ret;

  /* Check if `unget_list' is available */
  if (unget_list) {
    struct unget_list *old;

    ret = unget_list->c;

    old = unget_list;
    unget_list = unget_list->prev;
    free(old);

    goto end;
  }
  /* Otherwise, check if `unget_buf' is available */
  else if (unget_buf_index) {
    ret = unget_buf[--unget_buf_index];
    goto end;
  }

  if (command_string) {
    /* Make sure we're not at the end of `command_string'. */
    if (!command_string[command_string_index])
      return EOF;

    /* Get a charcter from `command_string'. */
    ret = command_string[command_string_index++];
    goto end;
  }

  if (interactive) {
    /* Get a character from `input_string' or get a new line if that's
       empty. */
    do {
#ifndef HAVE_LIBREADLINE
      size_t len = 0;
#endif
      char *prompt = PROMPT;
      
      if (input_string && input_string[input_string_index]) {
	ret = input_string[input_string_index++];
	goto end;
      }

      if (input_string) {
	char return_newline =
	  !*input_string || input_string[input_string_index - 1] != '\n';
	
	free(input_string);
	input_string = NULL;

	if (return_newline) {
	  input_string_index = 0;
	  ret = '\n';
	  goto end;
	}
      }

      input_string_index = 0;

#ifdef HAVE_LIBPYTHON
      /* Get the prompt if we're in shebang mode */
      if (shebang) {
	PyObject *pysys, *pyprompt;

	if ((pysys = PyImport_AddModule("sys")) &&
	    (pyprompt =
	     PyObject_GetAttrString(pysys, PROMPT_VAR))) {
	  PyObject *pyprompt_str;

	  /* Handle non-str objects */
	  if ((pyprompt_str = PyObject_Str(pyprompt))) {
	    PyObject *pybytes;

	    /* We're done with this */
	    Py_DECREF(pyprompt);

	    /* Convert the string object to a bytes object */
	    if ((pybytes =
		 PyUnicode_AsEncodedString(pyprompt_str, NULL, NULL))) {
	      /* Convert the bytes object to a char * */
	      if (!(prompt = PyBytes_AsString(pybytes))) {
		PyErr_Print();
		prompt = PROMPT;
	      }

	      Py_DECREF(pybytes);
	    }
	    else {
	      PyErr_Print();
	    }
	  }
	  else {
	    Py_DECREF(pyprompt);
	    PyErr_Print();
	  }
	}
	else {
	  PyErr_Clear();
	}
      }
#endif

#ifdef HAVE_LIBREADLINE
      if (editing) {
	input_string = readline(prompt);
      }
      else {
	size_t len = 0;
#endif
	fputs(prompt, stderr);

	/* Indent the prompt */
	for (unsigned int i = 0; i < indent_count * prompt_indent_size; i++)
	  putc(' ', stderr);

	if (getline(&input_string, &len, *input_files) == -1) {
	  free(input_string);
	  input_string = NULL;
	}
#ifdef HAVE_LIBREADLINE
      }
#endif
      
      if (!input_string)
	return EOF;

#ifdef HAVE_READLINE_HISTORY
      /* Add the line to history if it's not empty. */
      if (
# ifdef HAVE_LIBREADLINE
	  editing ? *input_string :
# endif
	  *input_string != '\n')
	add_history(input_string);
      /* Otherwise, if it is empty, end the conditional (similar to
	 Python's behavior). I know, it brings in a little whitespace
	 dependency in interactive mode, but it is needed because
	 otherwise you couldn't type things like `if (1);
	 print("foo");' (Python doesn't like things immediately after
	 an if or similar in Py_single_input (which is used for
	 interactive input) mode.) */
# ifdef HAVE_LIBPYTHON
      else
# endif
#else
# ifdef HAVE_LIBPYTHON
      if (
#  ifdef HAVE_LIBREADLINE
	  editing ? !*input_string :
#  endif
	  *input_string == '\n')
# endif /* HAVE_LIBPYTHON */
#endif /* HAVE_READLINE_HISTORY */
#ifdef HAVE_LIBPYTHON
	/* Only end the conditional if it's not a `do' loop */
	if (!do_indent_count || do_indent_count->count != indent_count)
	  end_conditional();
#endif
    } while (!*input_string);

    ret = input_string[input_string_index++];
    goto end;
  }

  /* Read a character from `*input_files'. */
  ret = getc(*input_files);

 end:
  /* Update location in file. */
  if (ret == '\n') {
    /* Save the column in case we unget_input() ret. */
    column_save = column;
    column = 0;
    line++;
  }
  else {
    column++;
  }

  return ret;
}

int unget_input(int c) {
  /* Update location in file. */
  if (c == '\n') {
    column = column_save;
    line--;
  }
  else {
    column--;
  }

  /* Implement our own ungetting rather than using ungetc() so we can
     unget an unlimited number of characters */

  /* Unget to a fixed-sized buffer. It is very unlikely that we will
     exceed the size of this buffer, but in case we do, we can also
     unget to a linked list. */
  if (unget_buf_index < sizeof(unget_buf)) {
    unget_buf[unget_buf_index++] = (char)c;
  }
  else {
    /* The fixed-sized buffer is full. We must use the linked list
       instead. */
    if (unget_list) {
      struct unget_list *new;

      if (!(new = malloc(sizeof(*new))))
	return EOF;

      new->prev = unget_list;
      unget_list = new;
    }
    else {
      if (!(unget_list = malloc(sizeof(*unget_list))))
	return EOF;

      unget_list->prev = NULL;
    }

    unget_list->c = (char)c;
  }

  return c;
}

#ifdef HAVE_LIBREADLINE
/* The readline completer. */
char * c2py_complete(const char *text, int state) {
  const char *const completion_tokens[] = {"import", "from", "as",
					   "for", "do", "while", "in",
					   "if", "elif", "else",
					   "switch", "case", "default",
					   "try", "except", "finally", "raise",
					   "del",
					   "global",
					   "class",
					   "return"};
  static const char *matches
    [sizeof(completion_tokens) / sizeof(*completion_tokens)];
  static int index;
  static size_t size;

  if (!state) {
    index = size = 0;
    
    for (int i = 0;
	 i < sizeof(completion_tokens) / sizeof(*completion_tokens); i++)
      if (!strncmp(text, completion_tokens[i], strlen(text)))
	matches[size++] = completion_tokens[i];
  }

  if (index >= size)
    return NULL;

  /* We have to strdup() the string because readline expects a
     malloc()'d string. */
  return strdup(matches[index++]);
}

/* A hook to indent the text. */
int indent_text(void) {
  char *indent_string;
  size_t indent_string_size;

  indent_string_size = sizeof(*indent_string) *
    (((indent_count
       /* The way switch statements are implemented, each `case' is a
	  Python if statement, so the indent count is wrong for the
	  prompt. This line is to fix that. */
       - switch_count
       ) * prompt_indent_size) + 1);
  indent_string = malloc(indent_string_size);
  memset(indent_string, ' ', indent_string_size - 1);
  indent_string[indent_string_size - 1] = '\0';

  /* Insert the text. */
  rl_insert_text(indent_string);

  free(indent_string);

  return 0;
}
#endif /* HAVE_LIBREADLINE */
