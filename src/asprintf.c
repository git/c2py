/************************************************************************\
 * C2py converts cpy code (with a C-like syntax) to Python code.        *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                 *
 *                                                                      *
 * This file is part of c2py.                                           *
 *                                                                      *
 * C2py is free software: you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * C2py is distributed in the hope that it will be useful,              *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with c2py.  If not, see <https://www.gnu.org/licenses/>.       *
\************************************************************************/

/* asprintf.c -- local implementation of asprintf() and vasprintf() in
   case they are not included in the standard library */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "asprintf.h"

#ifndef HAVE_ASPRINTF
/* A simple function which just calls vasprintf() on its arguments */
int asprintf(char **strp, const char *fmt, ...) {
  int ret;
  va_list ap;

  /* Call vasprintf() with our arguments */
  va_start(ap, fmt);
  ret = vasprintf(strp, fmt, ap);
  va_end(ap);

  return ret;
}

# ifndef HAVE_VASPRINTF
int vasprintf(char **strp, const char *fmt, va_list ap) {
  int size;
  va_list ap_copy;

  /* We'll be using this twice, so make a copy */
  va_copy(ap_copy, ap);

  /* Get the size to allocate and return if it's -1 */
  if ((size = vsnprintf(NULL, 0, fmt, ap_copy)) == -1)
    return -1;

  /* We're done with this now */
  va_end(ap_copy);

  size++; // For the terminating '\0'

  /* Now allocate the string and write to it */
  *strp = malloc(sizeof(**strp) * size);
  return vsnprintf(*strp, size, fmt, ap);
}
# endif /* ! HAVE_VASPRINTF */
#endif /* ! HAVE_ASPRINTF */
