/************************************************************************\
 * C2py converts cpy code (with a C-like syntax) to Python code.        *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                 *
 *                                                                      *
 * This file is part of c2py.                                           *
 *                                                                      *
 * C2py is free software: you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * C2py is distributed in the hope that it will be useful,              *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with c2py.  If not, see <https://www.gnu.org/licenses/>.       *
\************************************************************************/

/* python.h -- include <Python.h> with some c2py-specific stuff */

#ifndef _C2PY_PYTHON_H
#define _C2PY_PYTHON_H

#ifdef HAVE_LIBPYTHON
/* Save the state of HAVE_LIBREADLINE so we can reset it after
   including <Python.h> (which may define HAVE_LIBREADLINE in the
   platform-specific <pyconfig.h>). */
# ifdef HAVE_LIBREADLINE
#  define _HAVE_LIBREADLINE_BEFORE_PYTHON_H HAVE_LIBREADLINE
# endif

# include <Python.h>

/* Restore the state of HAVE_LIBREADLINE. */
# ifdef _HAVE_LIBREADLINE_BEFORE_PYTHON_H
#  ifndef HAVE_LIBREADLINE
#   define HAVE_LIBREADLINE _HAVE_LIBREADLINE_BEFORE_PYTHON_H
#  endif
#  undef _HAVE_LIBREADLINE_BEFORE_PYTHON_H
# else /* !_HAVE_LIBREADLINE_BEFORE_PYTHON_H */
#  undef HAVE_LIBREADLINE
# endif

/* Make sure our python version is greater than or equal to 3.3 */
# if PY_VERSION_HEX < 0x03030000
#  error "Sorry, your Python version must be 3.3 or higher"
# endif
#endif /* HAVE_LIBPYTHON */

#endif /* _C2PY_PYTHON_H */
