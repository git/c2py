/************************************************************************\
 * C2py converts cpy code (with a C-like syntax) to Python code.        *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                 *
 *                                                                      *
 * This file is part of c2py.                                           *
 *                                                                      *
 * C2py is free software: you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * C2py is distributed in the hope that it will be useful,              *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with c2py.  If not, see <https://www.gnu.org/licenses/>.       *
\************************************************************************/

/* parse.y -- bison input to generate the parser for c2py */

%{
  #define _GNU_SOURCE

  #ifdef HAVE_CONFIG_H
  # include <config.h>
  #endif

  #include "python.h" // Should be included first

  #include <stdio.h>
  #include <stdlib.h>
  #include <stdarg.h>
  #include <string.h>
  #include <ctype.h>
  #include <assert.h>
  #include <errno.h> // For program_invocation_name

  #include "c2py.h"
  #include "llist.h"
  #include "input.h"
  #include "asprintf.h"

  #define TOKEN_STRING "()[]{},=<>+-*/%@!&|^~.?:;" // Valid one-character tokens
  #define QUOTE_CHARS "\"'"

  /* Increment `num' and make sure it didn't wrap around, running
     `action' if it did */
  #define increment(num) do {						\
      if (!++(num)) {							\
	/* num has wrapped around to zero */				\
	fprintf(stderr, "%s: " __FILE__ ":" XSTR(__LINE__)		\
		": in %s(): " XSTR(num) " has overflowed\n",		\
		program_invocation_name, __func__);			\
	YYABORT;							\
      }									\
    } while (0)

  /* Make sure `num' is not 0, running `action' if it is, and
     decrement it */
  #define decrement(num) do {						\
      if (!(num)) {							\
	fprintf(stderr, "%s: " __FILE__ ":" XSTR(__LINE__)		\
		": in %s(): tried to decrement " XSTR(num)		\
		" below zero\n", program_invocation_name, __func__);	\
	YYABORT;							\
      }									\
									\
      (num)--;								\
    } while (0)

  /* Add a new element to a linked list */
  #define list_add_new(list, type) do {		\
      if (list) {				\
	type *new = malloc(sizeof(*new));	\
	new->prev = (list);			\
	(list) = new;				\
      }						\
      else {					\
	(list) = malloc(sizeof(*(list)));	\
	(list)->prev = NULL;			\
      }						\
    } while (0)

  /* Free an element from a linked list */
  #define list_free(list, type) do {		\
      type *old = (list);			\
      (list) = (list)->prev;			\
      free(old);				\
    } while (0)

  /* Add a new element to a doubly linked list */
  #define dlist_add_new(list) do {			\
      if (list) {					\
	(list)->next = malloc(sizeof(*((list)->next)));	\
	(list)->next->prev = (list);			\
	(list) = (list)->next;				\
      }							\
      else {						\
	(list) = malloc(sizeof(*(list)));		\
	(list)->prev = NULL;				\
      }							\
      (list)->next = NULL;				\
    } while (0)

  /* Free an element from a doubly linked list */
  #define dlist_free(list) do {			\
      if ((list)->prev) {			\
	(list) = (list)->prev;			\
	free((list)->next);			\
	(list)->next = NULL;			\
      }						\
      else {					\
	free(list);				\
	(list) = NULL;				\
      }						\
    } while (0)

  /* We have to define this here because we can't use preprocessor
     directives in the grammer rules section. */
  #define inc_indent_if_interactive()		\
    if (interactive) increment(indent_count)
  #define dec_indent_if_interactive()		\
    if (interactive) decrement(indent_count)

  struct backtrace {
    struct backtrace *prev, *next;
    enum { class, func, method } type;
    char *name;
    unsigned int line, column, indent_count;
  };

  struct quoted_pos {
    struct quoted_pos *prev;
    size_t start, end;
  };

  struct str_list {
    struct str_list *prev;
    char *str;
  };
 
  static void yyerror(const char *);
  static int yylex(void);
  static int indent(const char *, char *, size_t, size_t *);
  #ifndef HAVE_LIBPYTHON
  /* If we're compiled without libpython3, this will not be defined in
     input.h. */
  static int output(const char *, ...);
  #endif

  /* This is for error handling. */
  static unsigned int last_line = 1, last_column = 1;
  static unsigned int do_count = 0;
  /* Keep track of the backtrace for error reporting. */
  static struct backtrace *backtrace = NULL;
  /* Positions between which we should not indent because they are
     quoted. */
  static struct quoted_pos *quoted_pos = NULL;
  static char output_no_indent = 0,
    /* This is if we're in an empty `switch' so we know to output
       "pass" in a `case' */
    switch_init = 0;
%}

%define parse.error verbose

/* `string' is a string that is always dynamically allocated (with
   malloc());
   
   `static_string' is a string that is always statically allocated (on
   the stack);

   `opt_string' is a string that, if it is "", is statically
   allocated, otherwise it is dynamically allocated;

   `str_size' is a string and a size which is used to store how many
   elements are in a comma-separated list;

   `bool' is a boolean (currently used to check if various symbols
   were empty). */
%union {
  char *string, *static_string, *opt_string;
  struct {
    char *str;
    size_t size;
  } str_size;
  char bool;
}

/* Tokens */

%token STRING		"name"
%token STRING_LITERAL	"string literal"

%token IMPORT	"import"
%token FROM	"from"
%token AS	"as"

%token DO	"do"
%token WHILE	"while"
%token FOR	"for"
%token IN	"in"
%token IF	"if"
%token ELSE	"else"
%token ELIF	"elif"

%token SWITCH	"switch"
%token CASE	"case"
%token DEFAULT	"default"

%token TRY	"try"
%token EXCEPT	"except"
%token FINALLY	"finally"

%token RAISE	"raise"

%token POWER	"**"
%token INT_DIV	"//"

%token RSHIFT	">>"
%token LSHIFT	"<<"

%token PLUS_EQ		"+="
%token MINUS_EQ		"-*"
%token MUL_EQ		"*="
%token DIV_EQ		"/="
%token INT_DIV_EQ	"//="
%token MOD_EQ		"%="
%token MAT_MUL_EQ	"@="
%token AND_EQ		"&="
%token OR_EQ		"|="
%token XOR_EQ		"^="
%token RSHIFT_EQ	">>="
%token LSHIFT_EQ	"<<="
%token POWER_EQ		"**="

%token LESS_EQ		"<="
%token GREATER_EQ	">="
%token EQUAL		"=="
%token NOT_EQUAL	"!="

%token BOOL_AND	"&&"
%token BOOL_OR	"||"

%token DEL	"del"

%token GLOBAL	"global"

%token CLASS	"class"
%token RETURN	"return"

%token END 0	"end of file"

%token YYLEX_ERROR

/* Token types */

%type <string> STRING STRING_LITERAL

/* Symbol types */

%type <string> string_literal
%type <string> inheritance
%type <string> varname
%type <string> expr args dict_item
%type <string> import
%type <string> func_call

%type <opt_string> opt_expr except_expr opt_args
%type <opt_string> opt_from opt_assignment
%type <opt_string> stmt

%type <static_string> conditional_start_real conditional_start
%type <static_string> try_finally
%type <static_string> operation prefix
%type <static_string> varname_tokens

%type <str_size> varname_list

%type <bool> input input_single opt_input_single input_case input_single_case

/* Destructors */

%destructor { free($$); } <string>
%destructor { if (*$$) free($$); } <opt_string>
%destructor { free($$.str); } <str_size>

/* Printers (for trace output) */

%printer { fputs($$, yyo); } <string> <static_string> <opt_string>
%printer { fputs($$.str, yyo); } <str_size>
%printer { fputs($$ ? "empty" : "not empty", yyo); } <bool>

%initial-action {
		  indent_count = wait_string = switch_count = 0;
		  do_indent_count = NULL;

		  line = 1;
		  column = 0;
		  use_ps2 = 0;
		}

%%

/* This is to run `exec_str' when we're done parsing. */
start:		input { end_conditional(); }
	;

/* `$$' is whether the input was empty */
input:		%empty { $$ = 1; }
	|	input_single
	|	input input_single { $$ = $1 && $2; }
	;

/* An optional `input_single'. `$$' is whether the input was empty. */
opt_input_single:
		/* A single `;' is an empty statement */
		';' { $$ = 1; }
	|	input_single
	;

/* `$$' is whether the input was empty */
input_single:	stmt	    { output("%s\n", $1); free($1); $$ = 0; }
	|	func_def    { end_conditional(); $$ = 0; }
	|	class_def   { end_conditional(); $$ = 0; }
	|	sline_conditional { $$ = 0; }
	|	sline_for   { $$ = 0; }
	|	sline_do    { end_conditional(); $$ = 0; }
	|	sline_else  { end_conditional(); $$ = 0; }
	|	for	    { $$ = 0; }
	|	do	    { end_conditional(); $$ = 0; }
	|	conditional { $$ = 0; }
	|	else	    { $$ = 0; }
	|	switch	{ end_conditional(); $$ = 0; }
	|	try_except { $$ = 0; }
	|	STRING_LITERAL {
		  if (!minify) {
		    output("%s\n", $1);
		    $$ = 0;
		  }
		  else {
		    $$ = 1;
		  }

		  free($1);
		}
	|	yylex_error { $$ = 1; }
	|	error {
		  if (interactive)
		    $$ = 1;
		  else
		    YYABORT;
		}
	;

/* Either an `input' or a "pass" */
input_pass:	input { if ($1) output("pass\n"); }
	;

/* Either an `opt_input_single' or a "pass" */
input_single_pass:
		opt_input_single { if ($1) output("pass\n"); }
	;

/* An `input_single' or a `case' (used for `switch') */
input_single_case:
		input_single
	|	case { end_conditional(); $$ = 0; }
	;

/* Zero or more `input_single_case's */
input_case:	%empty { $$ = 1; }
	|	input_single_case {
		  if (!$1)
		    /* No need for "pass" anymore */
		    switch_init = 0;
		}
	|	input_case input_single_case { $$ = $1 && $2; }
	;

/* Either an `input_case' or a "pass" */
input_case_pass:
		input_case { if ($1) output("pass\n"); }
	;


stmt:		import	  { end_conditional(); $$ = $1; }
	|	func_call { end_conditional(); $$ = $1; }
	|	expr ';'  { end_conditional(); $$ = $1; }
	;

string_literal:	STRING_LITERAL
	|	string_literal string_literal {
		  asprintf(&$$, minify ? "%s%s" : "%s %s", $1, $2);

		  free($1);
		  free($2);
		}
	;

import:		IMPORT varname ';' {
		  asprintf(&$$, "import %s", $2);

		  free($2);
		}
	|	IMPORT varname AS STRING ';' {
		  asprintf(&$$, "import %s as %s", $2, $4);

		  free($2);
		  free($4);
		}
	|	FROM varname IMPORT STRING ';' {
		  asprintf(&$$, "from %s import %s", $2, $4);

		  free($2);
		  free($4);
		}
	|	FROM varname IMPORT STRING AS STRING ';' {
		  asprintf(&$$, "from %s import %s as %s", $2, $4, $6);

		  free($2);
		  free($4);
		  free($6);
		}
	;

func_call:	STRING '(' opt_args ')' ';' {
		  asprintf(&$$, "%s(%s)", $1, $3);

		  free($1);
		  if (*$3) free($3);
		}
	;

func_def:	STRING '(' opt_args ')' block_start {
		  output("def %s(%s):\n", $1, $3);

		  if (*$3) free($3);

		  /* Add the function to the backtrace. */
		  if (backtrace) {
		    backtrace->next = malloc(sizeof(*backtrace));
		    backtrace->next->prev = backtrace;
		    backtrace = backtrace->next;
		    backtrace->next = NULL;
		    backtrace->type = (backtrace->prev->type == class) ?
		      method : func;
		  }
		  else {
		    backtrace = malloc(sizeof(*backtrace));
		    backtrace->prev = NULL;
		    backtrace->next = NULL;
		    backtrace->type = func;
		  }
		  backtrace->name = $1;
		  backtrace->indent_count = indent_count;
		  backtrace->line = last_line;
		  backtrace->column = last_column;

		  increment(indent_count);
		} input_pass block_end
	;

/* Optional class inheritance. */
inheritance:	%empty { $$ = ""; }
	|	'(' opt_args ')' {
		  if (*$2) {
		    asprintf(&$$, "(%s)", $2);

		    free($2);
		  }
		  else {
		    $$ = "";
		  }
		}
	;

class_def:	CLASS STRING inheritance block_start {
		  output("class %s%s%s:\n",
			 $2, (!minify && *$3) ? " " : "", $3);

		  if (*$3) free($3);

		  /* Add the class to the backtrace. */
		  if (backtrace) {
		    backtrace->next = malloc(sizeof(*backtrace));
		    backtrace->next->prev = backtrace;
		    backtrace = backtrace->next;
		    backtrace->next = NULL;
		  }
		  else {
		    backtrace = malloc(sizeof(*backtrace));
		    backtrace->prev = NULL;
		    backtrace->next = NULL;
		  }
		  backtrace->type = class;
		  backtrace->name = $2;
		  backtrace->indent_count = indent_count;
		  backtrace->line = last_line;
		  backtrace->column = last_column;

		  increment(indent_count);
		} input_pass block_end
	;

conditional_start_real:
		IF	{ $$ = "if"; }
	|	ELIF	{ $$ = "elif"; }
	|	/* `while' has the same syntax as conditionals */
		WHILE	{ $$ = "while"; }
	;

conditional_start:
		conditional_start_real {
		  if (!indent_count)
		    wait_string = 1;

		  /* Increment `indent_count' so the prompt will be
		     properly indented. */
		  inc_indent_if_interactive();
		}
	|	/* Do our stuff RIGHT after we see the ELSE so
		   everything is indented properly for the prompt */
		else_start IF { $$ = "elif"; }
	;

/* The body of a single line conditional, while loop, etc. */
sline_body:	input_single_pass {
		  decrement(indent_count);
		}


/* This is so we can end `do's */
do_end_while:	WHILE '(' expr ')' ';' {
		  /* Whether we should increment `wait_string' after
		     we're done output()'ing the do loop */
		  char inc_wait_string = 0;

		  decrement(do_count);

		  if (wait_string == 1) {
		    wait_string = 0;
		    inc_wait_string = 1;
		  }

		  list_free(do_indent_count, struct indent_count);

		  /* Output the while expression */
		  output("while __c2py_do_%1$u__ or (%2$s):\n"
			 "%3$s__c2py_do_%1$u__%4$s\n",
			 do_count, $3, indent_string,
			 minify ? "=0" : " = False");

		  /* Clean up the temporary variable */
		  output("del __c2py_do_%u__\n", do_count);

		  wait_string += inc_wait_string;

		  free($3);
		}
	;

/* A single line conditional. We need to separate this out, because it
   acts as a single statement and does NOT set `block_empty'. */
sline_conditional:
		conditional_start '(' expr ')' {
		  /* Decrement `indent_count' if we incremented it for
		     the prompt. */
		  dec_indent_if_interactive();

		  output("%s %s:\n", $1, $3);

		  increment(indent_count);

		  free($3);
		} sline_body
	;

conditional:	conditional_start '(' expr ')' block_start {
		  /* Decrement `indent_count' if we incremented it for
		     the prompt. */
		  dec_indent_if_interactive();

		  output("%s %s:\n", $1, $3);

		  increment(indent_count);

		  free($3);
		} input_pass block_end
	;

for_start:	FOR {
		  if (!indent_count)
		    wait_string = 1;

		  /* Increment `indent_count' so the prompt will be
		     properly indented. */
		  inc_indent_if_interactive();
		}
	;

do_start:	DO {
		  if (!indent_count)
		    wait_string = 1;

		  /* Increment `indent_count' so the prompt will be
		     properly indented. */
		  inc_indent_if_interactive();
		}

else_start:	ELSE {
		  if (!indent_count)
		    wait_string = 1;

		  /* Increment `indent_count' so the prompt will be
		     properly indented. */
		  inc_indent_if_interactive();
		}
	;

sline_else:	else_start {
		  /* Decrement `indent_count' if we incremented it for
		     the prompt. */
		  dec_indent_if_interactive();

		  output("else:\n");

		  increment(indent_count);
		} sline_body
	;

else:		else_start block_start {
		  /* Decrement `indent_count' if we incremented it for
		     the prompt. */
		  dec_indent_if_interactive();

		  output("else:\n");
		  increment(indent_count);
		} input_pass block_end
	;

sline_for:	/* Python style syntax */
		for_start '(' varname IN args ')' {
		  /* Decrement `indent_count' if we incremented it for
		     the prompt. */
		  dec_indent_if_interactive();

		  output("for %s in %s:\n", $3, $5);

		  increment(indent_count);

		  free($3);
		  free($5);
		} sline_body
	|	/* C style syntax */
		for_start '(' opt_expr ';' opt_expr ';' opt_expr ')' {
		  /* Decrement `indent_count' if we incremented it for
		     the prompt. */
		  dec_indent_if_interactive();

		  if (*$3) {
		    output("%s\n", $3);
		    free($3);
		  }

		  output("while %s:\n", *$5 ? $5 : minify ? "1" : "True");

		  increment(indent_count);

		  if (*$5) free($5);
		}
		opt_input_single {
		  if (*$7) {
		    output("%s\n", $7);
		    free($7);
		  }
		  else if ($10) {
		    output("pass\n");
		  }

		  decrement(indent_count);
		}
	;

for:		/* Python style syntax */
		for_start '(' varname IN args ')' block_start {
		  /* Decrement `indent_count' if we incremented it for
		     the prompt. */
		  dec_indent_if_interactive();

		  output("for %s in %s:\n", $3, $5);

		  increment(indent_count);

		  free($3);
		  free($5);
		} input_pass block_end
	|	/* C style syntax */
		for_start '(' opt_expr ';' opt_expr ';' opt_expr ')'
		block_start {
		  /* Decrement `indent_count' if we incremented it for
		     the prompt. */
		  dec_indent_if_interactive();

		  if (*$3) {
		    /* Run this first. `wait_string' will be 2 greater
		       than indent_count since it is incremented in
		       `for_start' and `block_start' (I think). */
		    decrement(wait_string);
		    decrement(wait_string);
		    output("%s\n", $3);
		    increment(wait_string);
		    increment(wait_string);

		    free($3);
		  }
	  
		  output("while %s:\n", *$5 ? $5 : "True");

		  increment(indent_count);
	    
		  if (*$5) free($5);
		}
		input_pass {
		  if (*$7) {
		    output("%s\n", $7);
		    free($7);
		  }
		}
		block_end
	;

sline_do:	do_start {
		  /* Decrement `indent_count' if we incremented it for
		     the prompt. */
		  dec_indent_if_interactive();

		  list_add_new(do_indent_count, struct indent_count);
		  do_indent_count->count = indent_count;

		  /* Set the `do' variable to True */
		  decrement(wait_string);
		  output("__c2py_do_%u__%s\n",
			 do_count, minify ? "=1" : " = True");
		  increment(wait_string);

		  increment(do_count);
		  increment(indent_count);
		} input_single { decrement(indent_count); } do_end_while
	;

do:	        do_start block_start {
		  /* Decrement `indent_count' if we incremented it for
		     the prompt. */
		  dec_indent_if_interactive();

		  list_add_new(do_indent_count, struct indent_count);
		  do_indent_count->count = indent_count;

		  /* Set the `do' variable to True */
		  decrement(wait_string);
		  decrement(wait_string);
		  output("__c2py_do_%u__%s\n",
			 do_count, minify ? "=1" : " = True");
		  increment(wait_string);
		  increment(wait_string);

		  increment(do_count);
		  increment(indent_count);
		} input_pass block_end do_end_while
	;

/* Python does not natively support switch (or case) statements. This
   is a hack to add them in. */
switch:		SWITCH '(' expr ')' block_start {
		  output("while %s:\n",
			 minify ? "1" : "True");
		  
		  increment(indent_count);
		  
		  output("__c2py_switch_val_%1$u__%2$s%3$s\n"
			 "__c2py_switch_%1$u__%4$s\n"
			 /* Don't execute anything until the first
			    `case' (which will automatically exit the
			    `if False'). This is the behavior of
			    switch statements in C. */
			 "if %5$s:\n",
			 switch_count, minify ? "=" : " = ", $3,
			 minify ? "=0" : " = False\n",
			 minify ? "0" : "False");

		  free($3);

		  increment(switch_count);
		  increment(indent_count);
		  increment(wait_string);

		  switch_init = 1;
		}
		input_case_pass block_end {
 		  /* Decrement `switch_count' and decrement
		     `indent_count' an extra time since we're in
		     another block (the initial `if False' or a
		     case/default statement) which is not ended with a
		     `block_end'. */
		  /* Break out of the `while True' for the switch. */
		  if (!minify)
		    /* Separate the `break' if we're not minifying. */
		    output("\n");
		  output("break\n");

		  decrement(switch_count);
		  decrement(indent_count);
		  decrement(wait_string);

		  /* Clean up the temporary variables */
		  output("del __c2py_switch_%u__\n", switch_count);
		  output("del __c2py_switch_val_%u__\n", switch_count);
		}
	;

case:		CASE expr ':' {
		  /* Output "pass" if we're in an empty switch (for
		     the initial "if False:") */
		  if (switch_init) {
		    output("pass\n");
		    switch_init = 0;
		  }

		  /* Decrement to get out of the previous case
		     statement or the initial "if False:". */
		  decrement(indent_count);

		  output("if __c2py_switch_%1$u__ or "
			 "__c2py_switch_val_%1$u__%2$s(%3$s):\n",
			 switch_count - 1, minify ? "==" : " == ", $2);

		  free($2);

		  increment(indent_count);

		  output("__c2py_switch_%u__%s\n",
			 switch_count - 1, minify ? "=1" : " = True");
		} input_case /* We don't want `input_case_pass' here
				because the `if' statement already has
				a body */
	|	DEFAULT ':' {
		  /* Output "pass" if we're in an empty switch (for
		     the initial "if False:") */
		  if (switch_init) {
		    output("pass\n");
		    switch_init = 0;
		  }

		  /* Decrement to get out of the previous case
		     statement or the initial "if False:". */
		  decrement(indent_count);

		  output("if True:\n");

		  increment(indent_count);

		  output("__c2py_switch_%u__%sTrue\n",
			 switch_count - 1, minify ? "=" : " = ");
		} input_case /* We don't want `input_case_pass' here
				because the `if' statement already has
				a body */
	;

except_expr:	opt_expr
	|	expr AS STRING {
		  asprintf(&$$, "%s as %s", $1, $3);

		  free($1);
		  free($3);
		}
	;

/* Either TRY or FINALLY since they have the same syntax. */
try_finally:	TRY { $$ = "try"; if (!indent_count) wait_string = 1; }
	|	FINALLY { $$ = "finally"; if (!indent_count) wait_string = 1; }
	;

except:		EXCEPT { if (!indent_count) wait_string = 1; }
	;

try_except:	try_finally opt_expr ';' {
		  if (minify)
		    output("%s:%s\n", $1, *$2 ? $2 : "pass");
		  else
		    output("%s:\n%s%s\n",
			   $1, indent_string, *$2 ? $2 : "pass");

		  if (*$2) free($2);
		}
	|	try_finally block_start {
		  output("%s:\n", $1);

		  increment(indent_count);
		} input_pass block_end
	|	except except_expr opt_expr ';' {
		  if (*$2) {
		    if (*$3) {
		      if (minify)
			output("except %s:%s\n", $2, $3);
		      else
			output("except %s:\n%s%s\n", $2, indent_string, $3);
		    }
		    else {
		      /* The user probably meant "except: $2" rather
			 than "except $2: pass". */
		      if (minify)
			output("except:%s\n", $2);
		      else
			output("except:\n%s%s\n", indent_string, $2);
		    }

		    free($2);
		  }
		  else {
		    if (minify)
		      output("except:%s\n", *$3 ? $3 : "pass");
		    else
		      output("except:\n%s%s\n",
			     indent_string, *$3 ? $3 : "pass");
		  }

		  if (*$3) free($3);
		}
	|	except except_expr block_start {
		  if (*$2) {
		    output("except %s:\n", $2);
		    free($2);
		  }
		  else {
		    output("except:\n");
		  }

		  increment(indent_count);
		} input_pass block_end
	;

block_start:	'{' {
		  /* Let the symbol that uses block_start increment
		     indent_count so it can print things unindented
		     (e.g. func_def) */
		  /* Tell output() to wait for the whole block */
		  increment(wait_string);
		}
	;

block_end:	'}' {
		  decrement(wait_string);
		  decrement(indent_count);

		  if (backtrace && backtrace->indent_count == indent_count) {
		    /* Pop the backtrace. */
		    free(backtrace->name);
		    if (backtrace->prev) {
		      backtrace = backtrace->prev;
		      free(backtrace->next);
		      backtrace->next = NULL;
		    }
		    else {
		      free(backtrace);
		      backtrace = NULL;
		    }
		  }
		  
  #ifdef HAVE_LIBPYTHON
		  /* This will cause output() to run the command since
		     we're done with the block. */
		  if (shebang && !wait_string)
		    output("");
  #endif
		}
	;

operation:	'+'        { $$ = "+"; }
	|	'-'        { $$ = "-"; }
	|	'*'        { $$ = "*"; }
	|	'/'        { $$ = "/"; }
	|	'%'        { $$ = "%"; }
	|	'@'        { $$ = "@"; }
	|	'='        { $$ = "="; }
	|	'<'        { $$ = "<"; }
	|	'>'        { $$ = ">"; }
	|	'&'        { $$ = "&"; }
	|	'|'        { $$ = "|"; }
	|	'^'        { $$ = "^"; }
	|	POWER      { $$ = "**"; }
	|	INT_DIV    { $$ = "//"; }
	|	RSHIFT     { $$ = ">>"; }
	|	LSHIFT     { $$ = "<<"; }
	|	PLUS_EQ    { $$ = "+="; }
	|	MINUS_EQ   { $$ = "-="; }
	|	MUL_EQ     { $$ = "*="; }
	|	DIV_EQ     { $$ = "/="; }
	|	INT_DIV_EQ { $$ = "//="; }
	|	MOD_EQ     { $$ = "%="; }
	|	MAT_MUL_EQ { $$ = "@="; }
	|	AND_EQ     { $$ = "&="; }
	|	OR_EQ      { $$ = "|="; }
	|	XOR_EQ     { $$ = "^="; }
	|	RSHIFT_EQ  { $$ = ">>="; }
	|	LSHIFT_EQ  { $$ = "<<="; }
	|	POWER_EQ   { $$ = "**="; }
	|	LESS_EQ    { $$ = "<="; }
	|	GREATER_EQ { $$ = ">="; }
	|	NOT_EQUAL  { $$ = "!="; }
	|	EQUAL      { $$ = "=="; }
	|	BOOL_AND   { $$ = "and"; }
	|	BOOL_OR    { $$ = "or"; }
	;

prefix:		'+' { $$ = "+"; }
	|	'-' { $$ = "-"; }
	|	/* '.' so .5 means 0.5 (. 5 will also mean that, but
		   whatever). */
		'.' { $$ = "."; }
	|	'~'        { $$ = "~"; }
	;

/* These are tokens that could also act as variable names since they
   are not reserved words in python3. Why not make them reserved words
   in c2py? For compatibility with other python3 libraries
   (e.g. "foo.default"). */
varname_tokens:	do_start { $$ = "do"; dec_indent_if_interactive(); }
	|	SWITCH	{ $$ = "switch"; }
	|	CASE	{ $$ = "case"; }
	|	DEFAULT	{ $$ = "default"; }
	;

/* A string which may have dots interspersed, e.g. foo.bar.baz */
varname:	STRING
	|	varname_tokens { $$ = strdup($1); }
	|	varname '.' varname {
		  asprintf(&$$, "%s.%s", $1, $3);

		  free($1);
		  free($3);
		}
	;

/* One or more dictionary items such as "foo: bar" */
dict_item:	expr ':' expr {
		  asprintf(&$$, minify ? "%s:%s" : "%s: %s", $1, $3);

		  free($1);
		  free($3);
		}
	|	dict_item ',' dict_item {
		  asprintf(&$$, minify ? "%s,%s" : "%s, %s", $1, $3);

		  free($1);
		  free($3);
		}
	;

/* An optional FROM */
opt_from:	%empty { $$ = ""; }
	|	FROM expr {
		  /* Put a space before the `from' so we don't have to
		     check if it's empty */
		  asprintf(&$$, " from %s", $2);

		  free($2);
		}
	;

/* An optional expression (can be empty). */
opt_expr:	%empty { $$ = ""; }
	|	/* Set `$$' explicitly to get rid of the "type clash"
		   warning. */
		expr { $$ = $1; }
	;

/* A string or string literal with operations. */
expr:		varname
	|	string_literal
	|	/* Lists */
		'[' args ']' {
		  asprintf(&$$, "[%s]", $2);

		  if (*$2) free($2);
		}
	|	/* Tuples */
		'(' opt_args ')' {
		  asprintf(&$$, "(%s)", $2);

		  if (*$2) free($2);
		}
	|	/* Dictionaries. We use a different syntax than Python
		   because `{' and `}' are used for starting and
		   ending blocks of code. "Now wait a minute," I hear
		   you say, "you're using the same syntax as lists!
		   You can't do that!" Well, think about it: lists and
		   dictionaries will always be unambiguous because the
		   dictionary will always have an expression with a
		   colon like so: "foo: bar". I admit that it is
		   confusing and not the best solution, but try to
		   find another! They all have worse problems, so
		   there! */
		'[' dict_item ']' {
		  asprintf(&$$, "{%s}", $2);

		  if (*$2) free($2);
		}
	|	/* Put empty lists and dictionaries here so we can
		   make them have different syntaxes. The syntax for
		   an empty list is "[]" whereas the syntax for an
		   empty dictionary is "[:]". Of course, "list()" and
		   "dict()" work as well.  | /* Indices */
		'[' ']' {
		  $$ = strdup("[]");
		}
	|	'[' ':' ']' {
		  $$ = strdup("{}");
		}
	|	expr '[' expr ']' {
		  asprintf(&$$, "%s[%s]", $1, $3);

		  free($1);
		  free($3);
		}
	|	/* Variable declaration and possibly definition */
		varname varname_list opt_assignment {
		  if ($2.size > 1) {
		    if (*$3) {
		      asprintf(&$$, minify ? "%s=(%s(%s),)*%zu" :
			       "%s = (%s(%s),) * %zu",
			       $2.str, $1, $3, $2.size);
		    }
		    else {
		      asprintf(&$$, minify ? "%s=(%s(),)*%zu" :
			       "%s = (%s(),) * %zu",
			       $2.str, $1, $2.size);
		    }
		  }
		  else {
		    if (*$3) {
		      asprintf(&$$, minify ? "%s=%s(%s)" : "%s = %s(%s)",
			       $2.str, $1, $3);
		    }
		    else {
		      asprintf(&$$, minify ? "%s=%s()" : "%s = %s()",
			       $2.str, $1);
		    }
		  }

		  free($1);
		  free($2.str);
		  if (*$3) free($3);
		}
		/* These used to be in the `input' symbol; they're
		   here now so the can be used in single line
		   conditionals, loops, etc. */
	|	DEL expr {
		  asprintf(&$$, "del %s", $2);

		  free($2);
		}
	|	RETURN opt_expr {
		  asprintf(&$$, "return%s%s", *$2 ? " " : "", $2);

		  if (*$2) free($2);
		}
	|	GLOBAL args {
		  asprintf(&$$, "global %s", $2);

		  free($2);
		}
		/* Raise an exception */
	|	RAISE {
		  size_t size = sizeof(*$$) * 6;

		  /* Even though we could statically allocate it
		     (i.e. $$ = "raise"), we need to dynamically
		     allocate it so the destructor will handle it
		     properly */
		  $$ = malloc(size);
		  /* This makes GCC warn about "specified bound 5
		     equals source length" for some reason. I'm not
		     sure why that's a problem, but I guess we'll use
		     strcat instead (since there's really no point in
		     using strncat(3) if we pass a size of `size'
		     since it can write up to `size + 1' bytes to
		     `$$'). */
		  /* strncat($$, "raise", size - 1); */
		  strcat($$, "raise");
		}
	|	RAISE expr opt_from {
		  asprintf(&$$, "raise %s%s", $2, $3);

		  free($2);
		  if (*$3) free($3);
		}
	|	/* Parentheses for precedence */
		'(' expr ')' {
		  asprintf(&$$, "(%s)", $2);

		  free($2);
		}
	|	/* Function call */
		expr '(' opt_args ')' {
		  asprintf(&$$, "%s(%s)", $1, $3);

		  free($1);
		  if (*$3) free($3);
		}
	|	/* Class variable access */
		expr '.' varname {
		  asprintf(&$$, "%s.%s", $1, $3);

		  free($1);
		  free($3);
		}
	|	/* Method call */
		expr '.' varname '(' opt_args ')' {
		  asprintf(&$$, "%s.%s(%s)", $1, $3, $5);

		  free($1);
		  free($3);
		  if (*$5) free($5);
		}
	|	/* Variable operation */
		expr operation expr {
		  asprintf(&$$, (minify && !isalpha($2[0])) ?
			   "%s%s%s" : "%s %s %s", $1, $2, $3);

		  free($1);
		  free($3);
		}
	|	/* Sign, etc. */
		prefix expr {
		  asprintf(&$$, "%c%s", *$1, $2);

		  free($2);
		}
	|	expr IN args {
		  asprintf(&$$, "%s in %s", $1, $3);

		  free($1);
		  free($3);
		}
	|	/* ? : syntax */
		expr '?' expr ':' expr {
		  asprintf(&$$, "%s if %s else %s", $3, $1, $5);

		  free($1);
		  free($3);
		  free($5);
		}
	|	/* Boolean `not' */
		'!' expr {
		  asprintf(&$$, "not %s", $2);

		  free($2);
		}
	;

/* This is so we can have empty arguments without allowing things like
   `func(,,);' */
opt_args:	%empty { $$ = ""; }
		/* Set `$$' explicitly to get rid of the "type clash"
		   warning. */
	|	args { $$ = $1; }
	|	/* Comma separated lists allow an optional comma at
		   the end. I guess so one-member tuples can be
		   distinguished from parentheses for precedence. And
		   for the other places where comma separated lists
		   are applied, just to be consistent with the tuple
		   syntax (is my guess). There is probably more info
		   in the official Python documentation. */
		args ',' {
		  asprintf(&$$, "%s,", $1);

		  free($1);
		}
	;

args:		expr
	|	args ',' args {
		  asprintf(&$$, minify ? "%s,%s" : "%s, %s", $1, $3);

		  free($1);
		  free($3);
		}
	;

/* Like `args', but we store the number of arguments too and we only
   allow `varname's */
varname_list:	varname { $$.str = $1; $$.size = 1; }
	|	varname_list ',' varname {
		  asprintf(&$$.str, minify ? "%s,%s" : "%s, %s", $1.str, $3);

		  $$.size++;

		  free($1.str);
		  free($3);
		}
	;

/* Optional assignment */
opt_assignment:	%empty { $$ = ""; }
	|	'=' expr { $$ = $2; }
	;

/* A lexical error. */
yylex_error:	YYLEX_ERROR {
		  if (!interactive)
		    YYABORT;
		}
	;

%%

/* We have to put this in the epilogue so we can use the token macros */
static const struct {char *str; int token;} multi_char_tokens[] =
  {
   {"import",  IMPORT },
   {"from",    FROM   },
   {"as",      AS     },
   {"for",     FOR    },
   {"do",      DO     },
   {"while",   WHILE  },
   {"in",      IN     },
   {"if",      IF     },
   {"elif",    ELIF   },
   {"else",    ELSE   },
   {"switch",  SWITCH },
   {"case",    CASE   },
   {"default", DEFAULT},
   {"try",     TRY    },
   {"except",  EXCEPT },
   {"finally", FINALLY},
   {"raise",   RAISE  },
   {"del",     DEL    },
   {"global",  GLOBAL },
   {"class",   CLASS  },
   {"return",  RETURN }
  };

static void yyerror(const char *message) {
  fprintf(stderr, "%s:%u:%u", *input_file_names, last_line, last_column);

  if (backtrace) {
    /* Save the `backtrace' because we will overwrite it. */
    struct backtrace *backtrace_save = backtrace;
    
    /* Go to the beginning of `backtrace' and print it from there to
       be more like Python's "traceback"s (most recent call last). */
    while (backtrace->prev)
      backtrace = backtrace->prev;
    
    /* Print the backtrace. */
    while (backtrace) {
      fprintf(stderr, "\n%u:%u in %s %s", backtrace->line, backtrace->column,
	      (backtrace->type == class) ? "class" :
	      (backtrace->type == func) ? "function" : "method",
	      backtrace->name);
      
      backtrace = backtrace->next;
    }

    /* Restore the backtrace. */
    backtrace = backtrace_save;

    /* Now print the error message. */
    fprintf(stderr, "\n%s\n", message);
  }
  else {
    /* No need for a newline since we didn't print a backtrace. */
    fprintf(stderr, ": %s\n", message);
  }

#ifdef ENABLE_POLYRUN
  /* Print a sorry message if polyrun is installed (see polyrun(1) and
     polygen(1)). */
  system("polyrun sorry >&2 2> /dev/null");
#endif
}

/* Check if a character in a linked list of characters is escaped */
static int escaped(const llist_t *chars) {
  int is_escaped = 0;

  /* Check if the character is escaped */
  for (chars = chars->prev; chars && chars->data == '\\'; chars = chars->prev)
    is_escaped = !is_escaped;

  return is_escaped;
}

/* Check if a character in a string (not a linked list) is escaped */
static int escaped_str(const char *str, int index) {
  int is_escaped = 0;

  /* Check if the character is escaped */
  for (int i = index - 1; i >= 0 && str[i] == '\\'; i--)
    is_escaped = !is_escaped;

  return is_escaped;
}

/* Add a character to `input_chars' */
#define yylex_add_char(char) do {		\
    input_chars->data = c;			\
    llist_add_forward(input_chars);		\
    llist_next(input_chars);			\
  } while (0)

static int yylex(void) {
  int c, d;
  llist_t *input_chars = NULL;
  char quote = 0, mline_quote = 0;
  char string_prefix[3]; // A raw/formatted (bytes) string prefix

  /* Skip white space and comments. */
  while (1) {
    char seen_newline = 0;

    d = 0;

    while (isspace(c = get_input())) {
      if (!minify) {
	if (c == '\n') {
	  if (seen_newline)
	    output("\n");
	  else
	    seen_newline = 1;
	}
      }
    }

    if (c == EOF) {
      /* Make sure that all {'s are matched. */
      if (indent_count) {
	yyerror("syntax error, unmatched `{'");
	return YYLEX_ERROR;
      }
      
      return 0;
    }

    last_line = line;
    last_column = column;

    /* Skip comments. Note that we can't use // since that is used for
       integer division. */
    /* Multi-line comment. */
    if (c == '/') {
      d = c;
      c = get_input();

      if (c == EOF)
	return '/';
      
      if (c == '/') {
	/* Integer division */
	unget_input(c);
	/* `c' already is `d', so we need not set it */

	break;
      }

      /* It is a multi-line comment */
      if (c == '*') {
	/* Make the interactive prompt use `sys.ps2' */
	use_ps2 = 1;

	if (!minify)
	  output("#");

	while (1) {
	  d = c;
	  c = get_input();

	  /* Check for EOF. */
	  if (c == EOF) {
	    yyerror("syntax error, unterminated comment");

	    return YYLEX_ERROR;
	  }

	  /* Check for the end of the comment. */
	  if (c == '*') {
	    d = c;
	    c = get_input();

	    /* Check for EOF. */
	    if (c == EOF) {
	      yyerror("syntax error, unterminated comment");

	      return YYLEX_ERROR;
	    }

	    if (c == '/') {
	      break;
	    }
	    else {
	      unget_input(c);
	      c = d;
	    }
	  }

	  /* We haven't reached the end yet. */
	  if (!minify) {
	    output_no_indent = 1;
	    output("%c", c);
	    output_no_indent = 0;
	    
	    if (c == '\n') {
	      output("#");
	    }
	  }
	}

	if (!minify)
	  output("\n");

	/* Stop using `sys.ps2' */
	use_ps2 = 0;
      }
      else {
	unget_input(c);
	c = d;
	break;
      }
    }
    /* Single-line comment. EOF's are ok for single line comments
       because a single line comment without a '\n' at the end of a
       file is legitimate. */
    else if (c == '#') {
      /* Indent it correctly. */
      if (!minify)
	output("#");
      
      do {
	c = get_input();

	if (c != EOF && !minify) {
	  output_no_indent = 1;
	  output("%c", c);
	  output_no_indent = 0;
	}
      } while (c != '\n' && c != EOF);

      if (c != EOF)
	unget_input(c);
    }
    else {
      break;
    }
  }

  if (strchr(TOKEN_STRING, c)) {
    d = get_input();

    if (d == EOF)
      return c;
      
    /* Check for ==, +=, etc. */
    switch (c) {
    case '!':
      if (d == '=')
	return NOT_EQUAL;
      break;
    case '%':
      if (d == '=')
	return MOD_EQ;
      break;
    case '@':
      if (d == '=')
	return MAT_MUL_EQ;
      break;
    case '&':
      switch (d) {
      case '&':
	return BOOL_AND;
      case '=':
	return AND_EQ;
      }
      break;
    case '|':
      switch (d) {
      case '|':
	return BOOL_OR;
      case '=':
	return OR_EQ;
      }
      break;
    case '^':
      if (d == '=')
	return XOR_EQ;
      break;
    case '=':
      if (d == '=')
	return EQUAL;
      break;
    case '>':
      switch (d) {
	int e;

      case '>':
	if ((e = get_input()) == '=')
	  return RSHIFT_EQ;

	unget_input(e);
	return RSHIFT;
      case '=':
	return GREATER_EQ;
      }
      break;
    case '<':
      switch (d) {
	int e;

      case '<':
	if ((e = get_input()) == '=')
	  return LSHIFT_EQ;

	unget_input(e);
	return LSHIFT;
      case '=':
	return LESS_EQ;
      }
      break;
    case '+':
      switch (d) {
      case '+':
	/* Make `++' behave like `+= 1' */
	unget_input('1');
      
	return PLUS_EQ;
      case '=':
	return PLUS_EQ;
      }
      break;
    case '-':
      switch (d) {
      case '-':
	/* Make `--' behave like `-= 1' */
	unget_input('1');
      
	return MINUS_EQ;
      case '=':
	return MINUS_EQ;
      }
      break;
    case '*':
      switch (d) {
	int e;

      case '*':
	if ((e = get_input()) == '=')
	  return POWER_EQ;

	unget_input(e);
	return POWER;
      case '=':
	return MUL_EQ;
      }
      break;
    case '/':
      switch (d) {
	int e;

      case '/':
	if ((e = get_input()) == '=')
	  return INT_DIV_EQ;

	unget_input(e);
	return INT_DIV;
      case '=':
	return DIV_EQ;
      }
      break;
    case '.':
      if (d == '.') {
	int e = get_input();

	if (e == '.') {
	  /* Return an ellipsis */

	  /* We must dynamically allocate this because tokens of type
	     `STRING' are expected to be dynamically allocated.
	  
	     TODO: create a token `STATIC_STRING' which is statically
	     allocated, and use that instead. The only problem is: I
	     want them both to be referred to as "name" in error
	     messages, but you can't declare them both as
	     "name". There must be some way to do it... */
	  yylval.string = malloc(sizeof(*(yylval.string)) * 4);
	  strcpy(yylval.string, "...");
	  return STRING;
	}

	if (e != EOF)
	  unget_input(e);
      }
    }

    unget_input(d);

    return c;
  }

  /* Raw strings and formatted strings */
  if (strchr("fFrR", c)) {
    string_prefix[0] = c;

    c = get_input();

    /* Raw formatted strings and raw bytes strings */
    if (strchr(strchr("fF", string_prefix[0]) ? "rR" : "fFbB", c)) {
      string_prefix[1] = c;
      string_prefix[2] = '\0';

      c = get_input();
    }
    else {
      string_prefix[1] = '\0';
    }
  }
  /* Bytes strings */
  else if (strchr("bB", c)) {
    string_prefix[0] = c;

    c = get_input();

    /* Raw bytes strings */
    if (strchr("rR", c)) {
      string_prefix[1] = c;
      string_prefix[2] = '\0';

      c = get_input();
    }
    else {
      string_prefix[1] = '\0';
    }
  }
  else {
    string_prefix[0] = '\0';
  }

  /* Check if we're quoted. */
  if (strchr(QUOTE_CHARS, c))
    quote = c;

  llist_create(input_chars);

  /* Get the input string */
  while (quote || (c != EOF && !isspace(c) &&
		   !strchr(TOKEN_STRING QUOTE_CHARS, c))) {
    /* If c is '\0' (not EOF), that means the user was trying to
       confuse us. Obviously, the user mistook us for your average
       dumb program. We are smarter than that but still, not cool. */
    if (!c) {
      fputs("Hey! That's not nice! :(\n", stderr);
      llist_destroy(input_chars);

      return YYLEX_ERROR;
    }

    if (c == quote && input_chars->prev) {
      if (input_chars->prev->prev) {
	if (input_chars->prev->data == quote &&
	    input_chars->prev->prev->data == quote &&
	    !escaped(input_chars->prev->prev)) {
	  if (mline_quote) {
	    /* We're ending a triple-quoted string literal */

	    /* Stop using `sys.ps2' */
	    use_ps2 = 0;

	    break;
	  }

	  /* We're starting a triple-quoted string literal */

	  /* Make the interactive prompt use `sys.ps2' */
	  use_ps2 = 1;

	  mline_quote = 1;

	  /* Handle empty multi-line string literals. */
	  yylex_add_char(c);

	  c = get_input();
      
	  if (c == quote) {
	    c = get_input();

	    yylex_add_char(c);
	    
	    if (c == quote) {
	      c = get_input();
      
	      yylex_add_char(c);
	    
	      if (c == quote) {
		/* Stop using `sys.ps2' */
		use_ps2 = 0;

		break;
	      }
	    }
	  }
	}

	/* Break if we're ending a single line string literal */
	if (!mline_quote && !escaped(input_chars))
	  break;
      }
      /* Handle empty single-line string literals. */
      else if (input_chars->prev->data == quote) {
	unget_input(c = get_input());

	if (c == EOF) {
	  yyerror("syntax error, unexpected EOF when scanning string literal");
	  llist_destroy(input_chars);

	  return YYLEX_ERROR;
	}

	if (c != quote) {
	  c = quote;

	  break;
	}

	c = quote;
      }
    }

    yylex_add_char(c);

    c = get_input();

    if (quote) {
      if (c == EOF) {
	if (mline_quote)
	  yyerror("syntax error, unexpected end of file "
		  "when scanning triple-quoted string literal");
	else
	  yyerror("syntax error, unexpected end of "
		  "file when scanning string literal");

	llist_destroy(input_chars);

	return YYLEX_ERROR;
      }
      else if (c == '\n' && !escaped(input_chars) && !mline_quote) {
	yyerror("syntax error, unexpected end of "
		"line when scanning string literal");
	llist_destroy(input_chars);
	
	return YYLEX_ERROR;
      }
    }
  }

  /* Add the quote character. */
  if (quote) {
    yylex_add_char(c);
  }
  /* Make sure we catch one-character tokens such as '(', ')', ',',
     and ';'. */
  else if (c != EOF && !isspace(c)) {
    unget_input(c);
    c = quote;
  }

  /* Convert the list to a string. */

  /* Check if there is a string prefix which we want to apply even if
     it was not a string literal ("RBfoo" for example) */
  if (*string_prefix) {
    char *tmp = llist_to_array(input_chars);

    asprintf(&(yylval.string), "%s%s", string_prefix, tmp);

    free(tmp);
  }
  else {
    yylval.string = llist_to_array(input_chars);
  }

  /* Free the list. */
  llist_destroy(input_chars);

  /* Check if the string is a multi-character token unless it's a
     string literal. */
  if (!quote) {
    for (int i = 0;
	 i < sizeof(multi_char_tokens) / sizeof(*multi_char_tokens); i++) {
      if (!strcmp(yylval.string, multi_char_tokens[i].str)) {
	free(yylval.string);
	yylval.string = NULL;

	return multi_char_tokens[i].token;
      }
    }
  }

  return quote ? STRING_LITERAL : STRING;
}

/* We can only use this in yylex() so #undef it */
#undef yylex_add_char

/* Initialize `quoted_pos' from a string

   FIXME: This is less efficient than it should be because we parse
   for quotes twice (once in yylex()) */
static void quoted_init(const char *str) {
  enum { none, single, Double, /* Capital D to distinguish from the
				  `double' type */
	 triple_single, triple_double } quote_type = none;
  size_t length = strlen(str);

  for (size_t i = 0; i < length; i++) {
    if ((str[i] == '\'' || str[i] == '"') && !escaped_str(str, i)) {
      if (quote_type == none) {
	/* Check if it's triple-quoted */
	if (i + 2 < length && str[i+1] == str[i] && str[i+2] == str[i])
	  quote_type = str[i] == '\'' ? triple_single : triple_double;
	else
	  quote_type = str[i] == '\'' ? single : Double;

	/* Add it to the list of quoted positions */
	list_add_new(quoted_pos, struct quoted_pos);
	quoted_pos->start = i;
      }
      /* Check if we're ending a quote */
      else if ((quote_type == single && str[i] == '\'') ||
	       (quote_type == Double && str[i] == '"') ||
	       (i + 2 < length &&
		((quote_type == triple_single &&
		  str[i] == '\'' && str[i+1] == '\'' && str[i+2] == '\'') ||
		 (quote_type == triple_double &&
		  str[i] == '"' && str[i+1] == '"' && str[i+2] == '"')))) {
	if (quote_type == triple_single || quote_type == triple_double)
	  quoted_pos->end = i + 2;
	else
	  quoted_pos->end = i;
      }
    }
  }
}

/* Check if `pos' is quoted (we shouldn't indent it) */
static int quoted(size_t pos) {
  /* The position we are currently checking */
  struct quoted_pos *quoted_pos_cur = quoted_pos;

  while (quoted_pos_cur) {
    if (pos >= quoted_pos_cur->start && pos <= quoted_pos_cur->end)
      return 1;

    quoted_pos_cur = quoted_pos_cur->prev;
  }

  /* If we get here, `pos' wasn't quoted */
  return 0;
}

/* Add a character to `indent_str' or output() it */
#define indent_add_char(c) do {			\
    if (indent_str && index) {			\
      indent_str[(*index)++] = (c);		\
    }						\
    else {					\
      if (putc((c), output_file) == EOF)	\
	return -1;				\
    }						\
  } while (0)

/* Indent a string, returning the number of spaces added */
static int indent(const char *str, char *indent_str,
		  size_t size, size_t *index) {
  int ret = 0;

  for (size_t i = 0; i < size; i++) {
    /* If i == 0, we want to indent first. Wait until after we indent
       it. */
    if (i)
      indent_add_char(str[i]);

    /* We should only indent it if there is a newline if the newline
       is not the end of the string. This is because following calls
       to output() will add to the indent and not indent correctly. We
       also should not indent if we are inside a quoted string. */
    if (!i || (i != size - 1 && str[i] == '\n' && !quoted(i))) {
      /* Now add the correct number of spaces. */
      for (int j = 0; j < indent_count * indent_size; j++) {
	indent_add_char(' ');
	ret++;
      }
    }

    /* Now copy the character if we hadn't before. */
    if (!i)
      indent_add_char(str[i]);
  }

  /* Add the '\0' if we're outputing to a string */
  if (indent_str && index)
    indent_str[*index] = '\0';

  return ret;
}

/* We can only use this in indent(), so #undef it */
#undef indent_add_char

/* Output the string to output_file or run it with the embedded
   interpreter if available, preceding with the appropriate
   indenting */
#ifndef HAVE_LIBPYTHON
/* get_input() does not use output() if we're not compiled with
   libreadline. */
static
#endif
int output(const char *format, ...) {
  int size;
  char *str = NULL, *do_str_run = NULL;
  size_t do_str_run_size = 0;
  int ret;
  va_list ap;
  static unsigned int do_str_count = 0;
  static struct str_size_list {
    struct str_size_list *prev;
    char *str;
    size_t size, index;
  } *do_str = NULL;

  /* Parse the format string */
  va_start(ap, format);
  size = vasprintf(&str, format, ap);
  va_end(ap);

  /* Check for errors. */
  if (size < 0)
    return size;

  /* This is the number of characters so far */
  ret = size;

  /* Parse the string for quotes */
  quoted_init(str);

  /* Add a new member to `do_str' if we need to */
  if (do_str_count < do_count) {
    do_str_count++;
    list_add_new(do_str, struct str_size_list);
    do_str->str = NULL;
    do_str->size = 1; // 1 for the terminating '\0'
    do_str->index = 0;
  }
  else if (do_str_count > do_count) {
    do_str_run = do_str->str;
    do_str_run_size = do_str->size - 1;

    do_str_count--;
    list_free(do_str, struct str_size_list);
  }
  assert(do_str_count == do_count);

  /* Add the string to the current `do_str' if we need to */
  if (do_str) {
    /* Calculate the size of the string */
    do_str->size += size + do_str_run_size;

    if (!output_no_indent) {
      for (char *pos = str; pos && *pos;) {
	if (!quoted((pos - str) / sizeof(*pos)))
	  do_str->size += indent_count * indent_size;

	if ((pos = strchr(pos, '\n'))) pos++;
      }
    }

    if (!(do_str->str =
	  realloc(do_str->str, sizeof(*(do_str->str)) * do_str->size)))
      return -1;

    if (output_no_indent) {
      strncat(do_str->str, str, size);
      do_str->index += size;
    }
    else {
      ret += indent(str, do_str->str, size, &(do_str->index));
    }

    if (do_str_run) {
      /* Add `do_str_run' */
      strncat(do_str->str, do_str_run, do_str_run_size);
      do_str->index += do_str_run_size;
    }
  }
#ifdef HAVE_LIBPYTHON
  else if (shebang) {
    static char *exec_str = NULL; // The string to execute
    static size_t exec_str_size = 1; // 1 for the terminating '\0'
    static size_t index = 0;

    /* Calculate the size of the string */
    exec_str_size += size + do_str_run_size;

    if (!output_no_indent) {
      for (char *pos = str; pos && *pos;) {
	if (!quoted((pos - str) / sizeof(*pos)))
	  exec_str_size += indent_count * indent_size;

	if ((pos = strchr(pos, '\n'))) pos++;
      }
    }

    if (exec_str) {
      if (!(exec_str = realloc(exec_str, sizeof(*exec_str) * exec_str_size))) {
	free(str);
	return -1;
      }
    }
    else {
      if (!(exec_str = malloc(sizeof(*exec_str) * exec_str_size))) {
	free(str);
	return -1;
      }

      exec_str[0] = '\0';
    }

    if (output_no_indent) {
      strncat(exec_str, str, size);
      index += size;
    }
    else {
      /* Now copy `str' to `exec_str' but indent correctly. We can
	 safely assume that indent() did not return -1, because it
	 only does that when outputting to `output_file'. */
      ret += indent(str, exec_str, size, &index);
    }

    if (do_str_run) {
      /* Add `do_str_run' */
      strncat(exec_str, do_str_run, do_str_run_size);
      index += do_str_run_size;
    }

    /* Run the string if it's not the start of a block ("if foo:\n"
       for example) and it's not part of a block. This is because we
       need to run the whole block at the same time ("if foo:\n
       bar()\n") so PyRun_String() doesn't complain about an
       unexpected EOF. */
    if (!wait_string) {
      PyObject *pymain, *pyvars, *pyret;
      int error_occured = 0;
      
      assert(index == exec_str_size - 1);

      exec_str[index] = '\0';

      /* Get a reference to the __main__ module and get the variables
	 for `globals' and `locals' to pass to PyRun_String(). */
      if (!(pymain = PyImport_AddModule("__main__")) ||
	  !(pyvars = PyModule_GetDict(pymain))) {
	error_occured = 1;
      }
      /* Now run the string if it's not empty */
      else if (*exec_str) {
	if (!(pyret = PyRun_String(exec_str, interactive ?
				   Py_single_input : Py_file_input,
				   pyvars, pyvars))) {
	  PyErr_Print();
	
	  if (!interactive)
	    exit(1);
	}
	else {
	  Py_DECREF(pyret);
	}
      }

      /* Reset the static variables. */
      free(exec_str);
      exec_str = NULL;
      exec_str_size = 1;
      index = 0;

      /* Check if an error occured. */
      if (error_occured) {
	free(str);

	if (!interactive)
	  exit(1);

# ifdef HAVE_LIBREADLINE
	return -1;
# endif
      }
    }
  }
#endif /* HAVE_LIBPYTHON */
  else {
    /* Now print the string to the output file, indenting
       correctly if needed. */
    if (output_no_indent) {
      if (fputs(str, output_file) == EOF)
	return -1;
    }
    else {
      int tmp;

      if ((tmp = indent(str, NULL, size, NULL)) == -1) {
	if (do_str_run) free(do_str_run);
	return -1;
      }

      ret += tmp;
    }

    /* Print `do_str_run' */
    if (do_str_run && fputs(do_str_run, output_file) == EOF) {
      free(do_str_run);
      return -1;
    }
  }

  if (do_str_run) free(do_str_run);

  /* Free the list of quoted positions */
  while (quoted_pos)
    list_free(quoted_pos, struct quoted_pos);

  free(str);

  return ret;
}
