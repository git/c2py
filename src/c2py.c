/************************************************************************\
 * C2py converts cpy code (with a C-like syntax) to Python code.        *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                 *
 *                                                                      *
 * This file is part of c2py.                                           *
 *                                                                      *
 * C2py is free software: you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * C2py is distributed in the hope that it will be useful,              *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with c2py.  If not, see <https://www.gnu.org/licenses/>.       *
\************************************************************************/

/* c2py.c -- main source file */

#define _GNU_SOURCE

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include "python.h" // Should be included first

#include <stdio.h>
#include <stdio_ext.h>
#include <fcntl.h>
#include <stdlib.h>
#ifndef HAVE_LIBPYTHON
# include <unistd.h>
# include <inttypes.h>
#endif
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <errno.h>
#include <getopt.h>
#include <pwd.h>

#include "readline.h"
#include "c2py.h"
#include "input.h"
#include "parse.h" // For definition of yyparse()

#if defined(HAVE_LIBREADLINE) && defined(HAVE_READLINE_HISTORY)
static void c2py_write_history(void);

static char *hist_file = NULL;
static char hist_file_free = 0; // Whether to free() `hist_file'
#endif

int main(int argc, char **argv) {
  char force = 0; // Whether to overwrite without asking
  char input_files_free = 0; // Whether to free() `input_files'
  char output_shebang = 0; // Whether to output a shebang in the output file
  char no_set_interactive = 0,
    no_set_indent_size = 0,
    no_set_prompt_indent_size = 0;
#ifdef HAVE_LIBREADLINE
  char no_set_editing = 0;
#endif
  int return_val = 0;
  /* Put this outside of the following #ifndef because we also use it
     when writing a shebang line to the output file. */
  char *python_file = PYTHON_FILE;
#ifndef HAVE_LIBPYTHON
  pid_t python_pid = 0;
#endif
  char *output_file_name = NULL;
  int num_input_files;
  FILE **input_files_save = NULL;
  /* This is so we can set &input_file_names to a char * within the
     main stack frame even if were setting it in a different stack
     frame (an if block) (or something like that anyway). */
  char *stdin_str = "<stdin>";
  char *cmdline_str = "<command line>";

#ifdef HAVE_LIBREADLINE
  editing = 0;
#endif
  prompt_indent_size = 0;
  interactive = 0;
  indent_size = 0;
  minify = -1;
  shebang = 1;
  command_string_free = 0;
  command_string = NULL;

  /* Parse the options */
  while (1) {
    int c;
    static struct option long_options[] = {
      {"ask",            no_argument,       NULL, 'a'              },
      {"command",        required_argument, NULL, 'c'              },
      {"debug",          no_argument,       NULL, 'd'              },
      {"trace",          no_argument,       NULL, 't'              },
      {"editing",        no_argument,       NULL, 'e'              },
      {"force",	         no_argument,       NULL, 'f'              },
      {"indent-count",   required_argument, NULL, 'I'              },
      {"indent-prompt",  required_argument, NULL, 'p'              },
      {"interactive",    no_argument,       NULL, 'i'              },
      {"minify",         optional_argument, NULL, 'm'              },
      {"noediting",      no_argument,       NULL, 'n'              },
      {"no-editing",     no_argument,       NULL, 'n'              },
      {"nointeractive",  no_argument,       NULL, OPT_NOINTERACTIVE},
      {"no-interactive", no_argument,       NULL, OPT_NOINTERACTIVE},
      {"obfuscate",      optional_argument, NULL, 'O'              },
      {"output",         required_argument, NULL, 'o'              },
      {"python",         required_argument, NULL, 'x'              },
      {"shebang",        optional_argument, NULL, '!'              },
      {"help",           no_argument,       NULL, 'h'              },
      {"version",        optional_argument, NULL, 'v'              },
      {NULL,             0,                 NULL,  0               }
    };

    /* The `+' at the beginning of the option string makes
       getopt_long() stop parsing at the first non-option argument. We
       want this so that passing arguments to scripts works
       correctly. */
    if ((c = getopt_long(argc, argv, "+ac:defI:im::nO::o:p:tx:#::!::hv::",
			 long_options, NULL)) == -1)
      break;

    switch (c) {
      intmax_t prompt_indent_size_intmax, indent_size_intmax;
      char *endptr;

    case 'a':
      force = 0;
      break;
    case 'c':
      command_string = optarg;

      /* Stop parsing options after -c because they will be used for
	 the script's arguments. */
      goto getopt_done;
    case 'd':
    case 't':
#if YYDEBUG
      yydebug = 1;
#else
      fprintf(stderr,
	     "%s: warning: tracing support is disabled in this version\n",
	     argv[0]);
#endif
    case 'e':
#ifdef HAVE_LIBREADLINE
      editing = 1;
      no_set_editing = 1;
#else
      fprintf(stderr,
	      "%s: warning: line editing is disabled in this version\n",
	      argv[0]);
#endif
      
      break;
    case 'f':
      force = 1;
      break;
    case 'p':
      prompt_indent_size_intmax = strtoimax(optarg, &endptr, 0);

      if (*endptr) {
	fprintf(stderr, "%s: error: garbage at end of \"%s\": \"%s\"\n",
		argv[0], optarg, endptr);
	return 1;
      }

      if (prompt_indent_size_intmax < 0) {
	fprintf(stderr, "%s: error: prompt indent size cannot be negative\n",
		argv[0]);
	return 1;
      }

      if (prompt_indent_size_intmax > UINT_MAX) {
	fprintf(stderr, "%s: error: prompt ident size is to large (max %u)\n",
		argv[0], UINT_MAX);
	return 1;
      }

      prompt_indent_size = prompt_indent_size_intmax;
      no_set_prompt_indent_size = 1;

      break;
    case 'I':
      indent_size_intmax = strtoimax(optarg, &endptr, 0);

      if (*endptr) {
	fprintf(stderr, "%s: error: garbage at end of \"%s\": \"%s\"\n",
		argv[0], optarg, endptr);
	return 1;
      }

      if (indent_size_intmax < 1) {
	fprintf(stderr, "%s: error: indent size must be positive\n",
		argv[0]);
	return 1;
      }

      if (indent_size_intmax > UINT_MAX) {
	fprintf(stderr, "%s: error: ident size is to large (max %u)\n",
		argv[0], UINT_MAX);
	return 1;
      }

      indent_size = indent_size_intmax;
      no_set_indent_size = 1;

      break;
    case 'i':
      interactive = 1;
      no_set_interactive = 1;
      break;
    case 'n':
#ifdef HAVE_LIBREADLINE
      editing = 0;
      no_set_editing = 1;
#else
      fprintf(stderr,
	      "%s: warning: line editing is disabled in this version\n",
	      argv[0]);
#endif
      
      break;
    case 'm':
    case 'O':
      if (optarg && (!strcmp(optarg, "0") || !strcasecmp(optarg, "no")))
	minify = 0;
      else
	minify = 1;

      break;
    case 'o':
      if (output_file_name) {
	fprintf(stderr,
		"%s: error: only one output file may be specified\n",
		argv[0]);
	return 1;
      }

      output_file_name = optarg;
      shebang = 0; /* Don't run in shebang mode if an output file is
		      specified */
      break;
    case 'x':
      /* Set the python executable name. Note that this is different
	 than -# because it continues parsing options and does not
	 imply shebang mode (although it is useless when not in
	 shebang mode). */
      python_file = optarg;
      break;
    case '#':
      shebang = 1;

      if (optarg)
#ifdef HAVE_LIBPYTHON
	fprintf(stderr,
		"%s: warning: using embedded interpreter instead of `%s'\n",
		argv[0], optarg);
#else
	python_file = optarg;
#endif

      /* Stop parsing options after -# because they will be used for
	 the script's arguments. */
      goto getopt_done;	
    case '!':
      output_shebang = 1;

      if (optarg)
	python_file = optarg;

      break;
    case OPT_NOINTERACTIVE:
      interactive = 0;
      no_set_interactive = 1;
      break;
    case 'h':
      usage();
      return 0;
    case 'v':
      /* Output an appropriate version string */
      if (optarg) {
	size_t size = strlen(optarg);

	if (!strncasecmp(optarg, "extra full", size))
	  puts(PACKAGE_STRING_EXTRA_FULL);
	else if (!strncasecmp(optarg, "full", size))
	  puts(PACKAGE_STRING_FULL);
	else
	  puts(PACKAGE_STRING);
      }
      else {
	puts(PACKAGE_STRING);
      }
      
      return 0;
    case '?':
      fusage(stderr);
      return 1;
    default:
      fprintf(stderr,
	      "%s: error: getopt_long(3) returned `%c' (ASCII %hhd)\n",
	      argv[0], c, c);
      return -2;
    }
  }

 getopt_done:
  if (minify == -1)
    minify = shebang;

  /* Shift the args to get rid of the options. */
  argv[optind - 1] = argv[0];
  argv += optind - 1;
  argc -= optind - 1;

  /* Set `indent_size' and `prompt_indent_size' to default values if
     they weren't set explicitly */
  if (no_set_indent_size) {
    if (!no_set_prompt_indent_size)
      /* Set `prompt_indent_size' to `indent_size' since `indent_size'
	 was set explicitly, but `prompt_indent_size' was not */
      prompt_indent_size = indent_size;
  }
  else {
    if (no_set_prompt_indent_size) {
      /* If we're not in minify mode, set `indent_size' to
	 `prompt_indent_size' since `prompt_indent_size' was set
	 explicitly, but `indent_size' was not. If
	 `prompt_indent_size' is 0, however, set `indent_size' to
	 default because `indent_size' cannot be 0. */
      indent_size = minify ? INDENT_SIZE_MINIFY :
	prompt_indent_size ? prompt_indent_size : INDENT_SIZE;
    }
    else {
      /* Set both to the default values if neither we're set
	 explicitly */
      indent_size = minify ? INDENT_SIZE_MINIFY : INDENT_SIZE;
      prompt_indent_size = PROMPT_INDENT_SIZE;
    }
  }

  /* Now allocate and set `indent_string' according to
     `indent_size' */
  indent_string = malloc(sizeof(*indent_string) * (indent_size + 1));
  memset(indent_string, ' ', indent_size);
  indent_string[indent_size] = '\0';

  /* If were in shebang mode, run python3 and create a pipe to it or
     use libpython3 if that's available. */
  if (shebang) {
#ifdef HAVE_LIBPYTHON
    wchar_t *python_argv[argc];

    /* Set output_file_name appropriately */
    output_file_name = "<python3>";

    /* Set the script arguments. */
    for (int i = 0; i < argc; i++) {
      if (!(python_argv[i] = Py_DecodeLocale(argv[i], NULL))) {
	fprintf(stderr, "%s: failed to decode \"%s\": %m\n", argv[0], argv[i]);
	return 1;
      }
    }

    /* Initialize Python. */
    Py_SetProgramName(python_argv[0]);
    Py_Initialize();

    /* If we don't have any arguments (the only two cases is that
       either we're running a command string (with `-c') or we're
       reading from standard input), include argv[0]. */
    if (argc < 2)
      PySys_SetArgv(argc, python_argv);
    /* Otherwise, exclude argv[0] (our name, not the script's) from
       the arguments. */
    else
      PySys_SetArgv(argc - 1, python_argv + 1);

    /* Now free `python_argv'. */
    for (int i = 0; i < argc; i++)
      PyMem_RawFree(python_argv[i]);
    
#else /* !HAVE_LIBPYTHON */
    int pipefd[2];
    
    /* Create the pipe. */
    if (pipe2(pipefd, O_CLOEXEC) == -1) {
      fprintf(stderr, "%s: failed to pipe(): %m\n", argv[0]);
      return -1;
    }

    if ((python_pid = fork()) == -1) {
      fprintf(stderr, "%s: failed to fork(): %m\n", argv[0]);
      return -1;
    }

    if (!python_pid) {
      /* + 1 for the terminating NULL.
       * 
       * Note that we *must* use malloc() rather than static
       * allocation, because otherwise execvp() will fail with errno
       * EFAULT "Bad address." Also note that, unless I am mistaken,
       * we need not free `python_argv' because we allocated it after
       * we fork()'d, so it should be freed automatically when we die.
       */
      char **python_argv = malloc(sizeof(*python_argv) * (argc + 1));

      /* Make python3 read from the pipe. */
      dup2(pipefd[0], fileno(stdin));

      /* These are the arguments to pass to python3. */
      python_argv[0] = python_file;
      python_argv[1] = "-";

      /* <= rather than < so we include the terminating NULL. */
      for (int i = 2; i <= argc; i++)
	python_argv[i] = argv[i];

      /* Execute python3. */
      execvp(python_file, python_argv);

      /* If we're still here, we failed to execute python3. */
      free(python_argv);
      fclose(stdin);

      fprintf(stderr, "%s: failed to execute %s: %m\n",
	      argv[0], python_file);

      return 1;
    }
    else {
      int wstatus;
      size_t size;

      /* We are the parent. Make sure python was successfully
	 executed. */
      usleep(10000); /* Sleep a bit to prevent race conditions. */
      if (waitpid(python_pid, &wstatus, WNOHANG) && WIFEXITED(wstatus))
	return WEXITSTATUS(wstatus);

      /* Open the write end of pipefd as a stream and set output_file
	 to that stream */
      if (!(output_file = fdopen(pipefd[1], "w"))) {
	fprintf(stderr, "%s: failed to open a pipe as a stream: %m\n",
		argv[0]);

	return -1;
      }
      
      /* Set output_file_name appropriately */
      size = sizeof(*output_file_name) * (strlen(python_file) + 3);
      output_file_name = malloc(size);
      snprintf(output_file_name, size, "<%s>", python_file);

      /* Trap all the signals we can since Python might want to do
	 something with them. */
      for (int signum = 1; signum < 65; signum++)
	if (signum != SIGKILL && signum != SIGSTOP && signum != 32 &&
	    signum != 33 && signal(signum, SIG_IGN) == SIG_ERR)
	  fprintf(stderr, "%s: warning: cannot trap signal %d: %m\n",
		  argv[0], signum);
    }
#endif /* !HAVE_LIBPYTHON */
  }
  /* If `output_file_name' is NULL or "-", set it to "<stdout>" and
     set output_file to stdout. Otherwise open the output file. */
  else if (!output_file_name || !strcmp(output_file_name, "-")) {
    output_file_name = "<stdout>";
    output_file = stdout;
  }
  else {
    /* Check if the output file exists if we're running interactively. */
    if (!force) {
      if (!faccessat(AT_FDCWD, output_file_name, F_OK, AT_SYMLINK_NOFOLLOW)) {
	/* The file does exist */

	char repeat = 1;
	struct stat st;

	/* Check if the file is a character device, a FIFO, or a
	   socket in which case we won't be overwriting it, so we
	   needn't ask. */
	if (stat(output_file_name, &st) == -1) {
	  fprintf(stderr, "%s: could not stat %s: %m\n",
		  argv[0], output_file_name);

	  return 1;
	}

	if (!(S_ISCHR(st.st_mode) || S_ISFIFO(st.st_mode)
	      || S_ISSOCK(st.st_mode))) {
	  /* Check if it's a directory. */
	  if (S_ISDIR(st.st_mode)) {
	    fprintf(stderr, "%s: cannot write %s: %s\n",
		    argv[0], output_file_name, strerror(EISDIR));
	    return 1;
	  }
		  
	  while (repeat) {
	    int c;

	    printf("Overwrite \"%s\"? ", output_file_name);

	    switch (c = getchar()) {
	    case 'y':
	      repeat = 0;
	      break;
	    case 'n':
	    case EOF:
	      return 0;
	    default:
	      fputs("Please type yes or no\n", stderr);
	      break;
	    }

	    /* Purge the input buffer. */
	    __fpurge(stdin);
	  }
	}
      }
      else if (errno != ENOENT) {
	fprintf(stderr, "%s: failed to check if %s exists: %m\n",
		argv[0], output_file_name);
	return 1;
      }
    }

    /* Open the file and check for errors. */
    if (!(output_file = fopen(output_file_name, "w"))) {
      fprintf(stderr, "%s: error: could not open %s: %m\n",
	      argv[0], output_file_name);
      return 1;
    }
  }

  /* Now, if we're not running a command string (with -c), open the
     input file(s), set it to stdin if none were passed, or open only
     one input file if we're in shebang mode. */
  if (!command_string) {
    if (argc < 2) {
      num_input_files = 1;
      input_file_names = &stdin_str;
      input_files = &stdin;
    }
    else {
      num_input_files = shebang ? 1 : argc - 1;
      input_file_names = argv + 1;

      input_files = malloc(sizeof(*input_files) * num_input_files);
      input_files_free = 1; // Remember to free input_files.

      for (int i = 0; i < num_input_files; i++) {
	/* If the file name is "-", set it to "<stdin>" and set the
	   file to stdin. Note that "-" may be specified more than
	   once. This is ok, it is like cat(1). */
	if (!strcmp(input_file_names[i], "-")) {
	  input_file_names[i] = "<stdin>";
	  input_files[i] = stdin;
	}
	else {
	  /* Open the file and check for errors */
	  if (!(input_files[i] = fopen(input_file_names[i], "r"))) {
	    fprintf(stderr, "%s: error: could not open %s: %m\n",
		    argv[0], input_file_names[i]);
	    return 1;
	  }
	}
      }
    }
  }
  else {
    num_input_files = 1;
    input_file_names = &cmdline_str;
    input_files = NULL;
  }

  /* Print a shebang to the output file if requested. */
  if (output_shebang
#ifdef HAVE_LIBPYTHON
      && !shebang
#endif
      ) {
    struct stat st;
    fprintf(output_file, "#!%s%s\n",
	    /* If python_file is just the name of the command rather
	       than a path, prefix the shebang with "/usr/bin/env "
	       (plus -S if there are spaces in python_file) so it
	       looks in $PATH for the command. */
	    strchr(python_file, '/') ? "" : strchr(python_file, ' ') ?
	    "/usr/bin/env -S " : "/usr/bin/env ", python_file);

    /* Add an extra newline to separate the shebang from the rest of
       the file unless we're minifying. */
    if (!minify
#ifdef HAVE_LIBPYTHON
	/* If we're in shebang mode (running the script), there will
	   be no output file. */
	&& !shebang
#endif
	)
      putc('\n', output_file);

#ifdef HAVE_LIBPYTHON
    if (!shebang) {
#endif
      /* Also make the file executable if it is a regular file. */
      if (fstat(fileno(output_file), &st) == -1) {
	fprintf(stderr,
		"%s: warning: could not stat %s: %m\n"
		"%s: not making executable\n",
		argv[0], output_file_name, argv[0]);
	}
      else if (S_ISREG(st.st_mode)) {
	if (fchmod(fileno(output_file),
		   st.st_mode | S_IXUSR | S_IXGRP | S_IXOTH) == -1)
	  fprintf(stderr, "%s: warning: could not chmod %s: %m\n",
		  argv[0], output_file_name);
      }
#ifdef HAVE_LIBPYTHON
    }
#endif
  }

  /* Add a comment to the output file if we're not in minify mode. */
  if (!minify
#ifdef HAVE_LIBPYTHON
      && !shebang
#endif
      ) {
    fputs("############################################"
	  "##########################\n# This file was "
	  "generated by " PACKAGE_STRING ". Do not edit.", output_file);

    /* Note to self: figure out why this is 26 not 28. */
    for (int i = 0; i < 26 - strlen(PACKAGE_STRING); i++)
      fputc(' ', output_file);

    fputs("#\n##########################################"
	  "############################\n", output_file);

    if (fflush(output_file) == EOF) {
      fprintf(stderr, "%s: error: could not write %s: %m\n",
	      argv[0], output_file_name);
      return 1;
    }
  }

#ifndef HAVE_LIBPYTHON
  /* If we're in shebang mode, set sys.argv[0] (in the python script)
     to the name of the script. */
  if (shebang) {
    char *script_name;

    if (command_string) {
      if (argc > 1)
	script_name = argv[1];
      else
	script_name = argv[0];
    }
    else {
      script_name = *input_file_names;
    }

    if (!minify)
      /* Separate it from the comment. */
      putc('\n', output_file);
    
    fprintf(output_file,
	    "from sys import argv as __c2py_argv__\n__c2py_argv__[0]%s\"",
	    minify ? "=" : " = ");

    for (int i = 0; i < strlen(script_name); i++) {
      if (isprint(script_name[i])) {
	if (script_name[i] == '"')
	  putc('\\', output_file);

	putc(script_name[i], output_file);
      }
      else {
	switch (script_name[i]) {
	case '\f':
	  fputs("\\f", output_file);
	  break;
	case '\n':
	  fputs("\\n", output_file);
	  break;
	case '\r':
	  fputs("\\r", output_file);
	  break;
	case '\t':
	  fputs("\\t", output_file);
	  break;
	case '\v':
	  fputs("\\v", output_file);
	  break;
	default:
	  fprintf(output_file, "\\%hho", script_name[i]);
	  break;
	}
      }
    }

    fputs("\"\ndel __c2py_argv__\n", output_file);
  }
  else
#endif /* !HAVE_LIBPYTHON */
  if (
#ifdef HAVE_LIBPYTHON
      /* We have to test this because it's just an `if', not an
	 `else if'. */
      !shebang &&
#endif
      command_string && argc > 1) {
    fprintf(stderr, "%s: warning: script argument%s",
	    argv[0], (argc > 2) ? "s" : "");

    for (int i = 1; i < argc; i++)
      fprintf(stderr, "%s \"%s\"", (i == 1) ? "" : ",", argv[i]);

    fprintf(stderr, " ha%s no effect\n", (argc > 2) ? "ve" : "s");
  }

  if (input_files_free)
    input_files_save = input_files;

  /* Now do the actual parsing. */
  for (int i = 0; i < num_input_files; i++) {
    if (!command_string) {
      if (!no_set_interactive
#ifdef HAVE_LIBREADLINE
	  || !no_set_editing
#endif
	  ) {
#ifdef HAVE_LIBREADLINE
	int tty
#else
	interactive
#endif
	  = isatty(fileno(*input_files)); // Whether we're on a tty

	if (!
#ifdef HAVE_LIBREADLINE
	    tty
#else
	    interactive
#endif
	    && errno != EINVAL && errno != ENOTTY)
	  fprintf(stderr, "%s: warning: failed to check if %s is a tty: %m\n",
		  argv[0], *input_file_names);

#ifdef HAVE_LIBREADLINE
	/* Set line editing and interactive mode depending on whether
	   we're on a tty */
	if (!no_set_interactive)
	  interactive = tty;

	if (!no_set_editing)
	  editing = tty;
#endif
      }

#ifdef HAVE_LIBREADLINE
      if (editing) {
	char *hist_size_str;
	
	if (!hist_file) {
	  /* Initialize the GNU Readline library. */
	  rl_readline_name = PACKAGE_NAME;
	  rl_basic_word_break_characters = " \f\n\r\t\v([{,=<>+-/*?:!&|^;\"'";
	  rl_completion_entry_function = c2py_complete;
	  rl_startup_hook = indent_text;

# ifdef HAVE_READLINE_HISTORY
	  /* Initialize the GNU History library. */
	  using_history();

	  /* Get the history file name from the environment or use
	     HISTFILE_DEFAULT under the user's home directory. */
	  if (!(hist_file = getenv(ENV_HISTFILE))) {
	    char *home; // The user's home directory
	    size_t hist_file_size;

	    /* Check $HOME first so the user has a chance to override
	       the home directory If the environment $HOME is unset or
	       empty, get the user's home from his/her entry in
	       /etc/passwd */  
	    if (!(home = getenv("HOME")) || home[0] == '\0') {
	      struct passwd *entry; // The user's entry in /etc/passwd

	      if (!(entry = getpwuid(getuid())) || entry->pw_dir[0] == '\0') {
		char cwd[PATH_MAX]; // The name of the current directory

		/* Fall back to the current directory. */
		if (!(home = getcwd(cwd, PATH_MAX)))
		  /* If we couldn't even get the current directory,
		     use /. */
		  home = "/";
	      }
	    }

	    hist_file_free = 1;
	    
	    hist_file_size = sizeof(*hist_file) *
	      (strlen(home) + strlen(HISTFILE_DEFAULT) + 2);
	    hist_file = malloc(hist_file_size);
	    snprintf(hist_file, hist_file_size, "%s/%s",
		     home, HISTFILE_DEFAULT);
	  }

	  /* Get the history file size (0 or unspecified meaning
	     infinite). */
	  if ((hist_size_str = getenv(ENV_HISTSIZE))) {
	    char *endptr;
	    int hist_size;

	    errno = 0;
	    hist_size = strtol(hist_size_str, &endptr, 0);
	    
	    if (errno) {
	      fprintf(stderr,
		      "%s: warning: could not parse environment variable "
		      ENV_HISTSIZE " (\"%s\"): %m\n", argv[0], hist_size_str);
	    }
	    else {
	      if (*endptr)
		fprintf(stderr, "%s: warning: garbage found at end of "
			"environment variable " ENV_HISTSIZE ": \"%s\"\n",
			argv[0], endptr);

	      /* Stifle the history and truncate the history file if
		 hist_size isn't 0. If it is zero, leave everything
		 unlimited. */
	      if (hist_size) {
		stifle_history(hist_size);

		if (history_truncate_file(hist_file, hist_size) &&
		    errno != ENOENT)
		  fprintf(stderr, "%s: warning: could not truncate %s: %m\n",
			  argv[0], hist_file);
	      }
	    }
	  }

	  /* Write history on exit. */
	  if (atexit(c2py_write_history))
	    fprintf(stderr, "%s: warning: could not set exit function\n",
		    argv[0]);

	  /* Read the history file. */
	  if (read_history(hist_file) && errno != ENOENT)
	    fprintf(stderr, "%s: warning: could not read %s: %m\n",
		    argv[0], hist_file);
# endif /* HAVE_READLINE_HISTORY */
	}
      
	rl_instream = *input_files;
      }
#endif /* HAVE_LIBREADLINE */

	  /* Print a version message to stderr like python(1) and
	     python(3) if we're in shebang and interactive mode. */
      if (shebang && interactive)
#ifdef HAVE_LIBPYTHON
	fprintf(stderr, PACKAGE_STRING_FULL "\n"
		"Python %s\n", Py_GetVersion());
#else
	fputs(PACKAGE_STRING_FULL "\n", stderr);
#endif
    }
    
    if (!minify
#ifdef HAVE_LIBPYTHON
	&& !shebang
#endif
	)
      fputc('\n', output_file);

    /* If there is more than one input file and we're not in minify
       mode, add a comment to the output file showing where each input
       file starts. */
    if (num_input_files > 1 && !minify
#ifdef HAVE_LIBPYTHON
	&& !shebang
#endif
	) {
      char file_name[26];

      if (strlen(*input_file_names) > 25) {
	strncpy(file_name, *input_file_names, 22);

	for (int i = 22; i < 25; i++)
	  file_name[i] = '.';

	file_name[25] = '\0';
      }
      else {
	strncpy(file_name, *input_file_names, 26);
      }

      fprintf(output_file, "#############################"
	      "#########################################\n"
	      "# The following was generated from the file %s",
	      file_name);

      for (int i = 0; i < 25 - strlen(file_name); i++)
	fputc(' ', output_file);

      fputs("#\n##########################################"
	    "############################\n", output_file);
    }

    /* Now call the parsing function generated from parse.y */
    return_val = yyparse();

    /* We can close the file now, but only if it's not stdin. */
    if (!command_string && *input_files != stdin &&
	fclose(*input_files) == EOF) {
      fprintf(stderr, "%s: error when closing %s: %m\n",
	      argv[0], *input_file_names);
      return_val = 1;
    }

    if (return_val) {
#ifndef HAVE_LIBPYTHON
      /* Kill Python if we started it. */
      if (shebang) {
	int wstatus = 0;

	/* First send it SIGTERM. */
	if (kill(python_pid, SIGTERM) == -1) {
	  fprintf(stderr, "%s: unable to kill %s: %m\n",
		  argv[0], python_file);
	  return -1;
	}

	/* Give Python 100 ms to die. */
	for (int i = 0; i < 100; i++) {
	  waitpid(python_pid, &wstatus, WNOHANG);

	  if (WIFEXITED(wstatus))
	    break;

	  usleep(1000);
	}

	if (!WIFEXITED(wstatus)) {
	  /* Kill Python with SIGKILL since it refuses to die. */
	  if (kill(python_pid, SIGKILL) == -1) {
	    fprintf(stderr, "%s: unable to kill %s: %m\n",
		    argv[0], python_file);
	    return -1;
	  }
	}
      }
#endif /* !HAVE_LIBPYTHON */

      return return_val;
    }

    /* Increment input_files and input_file_names. */
    input_files++;
    input_file_names++;
  }

  /* Free the input_files array if we allocated it (we need to use
     input_files_save because we incremented input_files). */
  if (input_files_free)
    free(input_files_save);

  free(indent_string);

  /* Close the output_file if it's not stdout or stderr. */
  if (
#ifdef HAVE_LIBPYTHON
      !shebang &&
#endif
      output_file != stdout && output_file != stderr &&
      fclose(output_file) == EOF) {
    fprintf(stderr, "%s: error when closing %s: %m\n",
	    argv[0], output_file_name);
    return 1;
  }

  if (shebang) {
#ifndef HAVE_LIBPYTHON
    int wstatus = 0;

    /* Free output_file_name if we allocated it (we only allocate it in
       shebang mode when we're not using libpython3) */
    free(output_file_name);

    /* Wait until python3 dies. */
    do
      waitpid(python_pid, &wstatus, 0);
    while (!WIFEXITED(wstatus));

    /* Return whatever python3 returned. */
    return WEXITSTATUS(wstatus);
#else /* HAVE_LIBPYTHON */
# if PY_VERSION_HEX < 0x03060000
    Py_Finalize();
# else
    if (Py_FinalizeEx() == -1)
      return 120;
# endif
#endif /* HAVE_LIBPYTHON */
  }

  return 0;
}

#if defined(HAVE_LIBREADLINE) && defined(HAVE_READLINE_HISTORY)
/* This function will write the history file. It is meant to be passed
   to atexit(3). */
static void c2py_write_history(void) {
  if (write_history(hist_file))
    fprintf(stderr, "%s: warning: could not write %s: %m\n",
	    program_invocation_name, hist_file);

  if (hist_file_free)
    free(hist_file);
}
#endif
