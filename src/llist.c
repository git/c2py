/************************************************************************\
 * C2py converts cpy code (with a C-like syntax) to Python code.        *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                 *
 *                                                                      *
 * This file is part of c2py.                                           *
 *                                                                      *
 * C2py is free software: you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * C2py is distributed in the hope that it will be useful,              *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with c2py.  If not, see <https://www.gnu.org/licenses/>.       *
\************************************************************************/

/* llist.c -- functions for operating on linked lists */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include "llist.h"

void llist_add_forward(llist_t *list) {
  llist_t *old_next = list->next;
  
  llist_create(list->next);
  list->next->prev = list;

  if (old_next) {
    list->next->next = old_next;
    list->next->next->prev = list->next;
  }
}

void llist_add_backward(llist_t *list) {
  llist_t *old_prev = list->prev;

  llist_create(list->prev);
  list->prev->next = list;

  if (old_prev) {
    list->prev->prev = old_prev;
    list->prev->prev->next = list->prev;
  }
}

void llist_destroy(llist_t *list) {
  llist_move_start(list);
  
  while (list->next) {
    llist_next(list);
    free(list->prev);
  }
  
  free(list);
}

/* This is implemented as a function rather than a macro because if it
   were a macro, then if, for example, you called
   llist_remove(list->prev), then it would expand to (omitting the ifs
   since we are assuming it is not on either of the ends):
       /\* NOTE: This is the same as list->prev = list->prev->prev *\/
	   list->prev->next->prev = list->prev->prev;
       /\* NOTE: list->prev is the old list->prev->prev because of the
	   last line, and we shouldn't even be touching (the old)
	   list->prev->prev->prev *\/
       list->prev->prev->next = list->prev->next;
       free(list);
   which would have disastrous consequences. */
void llist_remove(llist_t *list) {
  if (list->next)
    list->next->prev = list->prev;
  if (list->prev)
    list->prev->next = list->next;
  free(list);
}

/* Convert a linked list to an array */
char * llist_to_array(llist_t *list) {
  size_t size = 1;
  char *array;

  /* Make sure `list' is not NULL */
  if (!list)
    return NULL;

  /* Get the size of the linked list (go to the end and move backwards
     so that we can move forwards when copying to the array) */
  llist_move_end(list);
  while (list->prev) {
    size++;
    llist_prev(list);
  }

  /* Allocate memory for the array */
  if (!(array = malloc(size * (sizeof(*array)))))
    return NULL;

  /* Copy the linked list to the array */
  for (size_t i=0; i<size; i++) {
    array[i] = list->data;
    llist_next(list);
  }

  if (list)
    /* `list' should now be NULL, i.e. we should never reach this */
    return (char *)-1;

  return array;
}
