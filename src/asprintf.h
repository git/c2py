/************************************************************************\
 * C2py converts cpy code (with a C-like syntax) to Python code.        *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                 *
 *                                                                      *
 * This file is part of c2py.                                           *
 *                                                                      *
 * C2py is free software: you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * C2py is distributed in the hope that it will be useful,              *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with c2py.  If not, see <https://www.gnu.org/licenses/>.       *
\************************************************************************/

/* asprintf.h -- local implementation of asprintf() and vasprintf() in
   case they are not included in the standard library */

/* I doubt it, but some systems may have an <asprintf.h>, so it's best
   to be safe */
#ifndef _C2PY_ASPRINTF_H
#define _C2PY_ASPRINTF_H

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#ifndef HAVE_VASPRINTF
# include <stdarg.h> /* For va_list */
#endif

#ifndef HAVE_ASPRINTF
int asprintf(char **, const char *, ...);
# ifndef HAVE_VASPRINTF
int vasprintf(char **, const char *, va_list);
# endif /* ! HAVE_VASPRINTF */
#endif /* ! HAVE_ASPRINTF */

#endif /* ! _C2PY_ASPRINTF_H */
