/************************************************************************\
 * C2py converts cpy code (with a C-like syntax) to Python code.        *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                 *
 *                                                                      *
 * This file is part of c2py.                                           *
 *                                                                      *
 * C2py is free software: you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * C2py is distributed in the hope that it will be useful,              *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with c2py.  If not, see <https://www.gnu.org/licenses/>.       *
\************************************************************************/

/* readline.h -- include readline headers with some c2py-specific stuff */

#ifndef _C2PY_READLINE_H
#define _C2PY_READLINE_H

/* Include readline headers */
#ifdef HAVE_LIBREADLINE
# if defined(HAVE_READLINE_READLINE_H)
#  include <readline/readline.h>
# elif defined(HAVE_READLINE_H)
#  include <readline.h>
# else
extern char * readline(const char *);
# endif

/* RETURN may be defined in <readline/chardefs.h> which causes the
   compiler to balk in parse.h when `enum yytokentype' (with one of
   the tokens being RETURN) is defined. So we undefine it here. */
# ifdef RETURN
#  undef RETURN
# endif

/* Include history headers */
# ifdef HAVE_READLINE_HISTORY
#  if defined(HAVE_READLINE_HISTORY_H)
#   include <readline/history.h>
#  elif defined(HAVE_HISTORY_H)
#   include <history.h>
#  else
extern void add_history(const char *);
extern int write_history(const char *);
extern int read_history(const char *);
#  endif
# endif /* HAVE_READLINE_HISTORY */
#endif /* HAVE_LIBREADLINE */

#endif /* _C2PY_READLINE_H */
