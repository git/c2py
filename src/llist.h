/************************************************************************\
 * C2py converts cpy code (with a C-like syntax) to Python code.        *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                 *
 *                                                                      *
 * This file is part of c2py.                                           *
 *                                                                      *
 * C2py is free software: you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * C2py is distributed in the hope that it will be useful,              *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with c2py.  If not, see <https://www.gnu.org/licenses/>.       *
\************************************************************************/

/* llist.h -- function definitions and macros for operating on linked lists */

#ifndef _LLIST_H
#define _LLIST_H

#include <stdlib.h>

#define llist_new() calloc(1, sizeof(llist_t))
#define llist_create(list) ((list) = llist_new())
#define llist_next(list) ((list) = (list)->next)
#define llist_prev(list) ((list) = (list)->prev)
#define llist_move_start(list)	\
  while ((list)->prev)		\
    llist_prev(list)
#define llist_move_end(list)	\
  while ((list)->next)		\
    llist_next(list)

typedef struct _linked_list {
  struct _linked_list *prev, *next;
  char data;
} llist_t;

void llist_add_forward(llist_t *list);
void llist_add_backward(llist_t *list);
void llist_destroy(llist_t *list);
void llist_remove(llist_t *list);
char * llist_to_array(llist_t *list);

#endif /* !_LLIST_H */
