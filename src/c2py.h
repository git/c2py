/************************************************************************\
 * C2py converts cpy code (with a C-like syntax) to Python code.        *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                 *
 *                                                                      *
 * This file is part of c2py.                                           *
 *                                                                      *
 * C2py is free software: you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * C2py is distributed in the hope that it will be useful,              *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with c2py.  If not, see <https://www.gnu.org/licenses/>.       *
\************************************************************************/

/* c2py.h -- main header file */

#ifndef _C2PY_H
#define _C2PY_H

#include <stdio.h>
#include <limits.h>

#define STR(x) #x
#define XSTR(x) STR(x)

/* The name of the compiler. */
#if defined __GNUC__
# define COMPILER "GCC"
#elif defined __clang__
# define COMPILER "Clang"
#else
# define COMPILER "UNKNOWN"
#endif

/* Supported libraries */
#ifdef HAVE_LIBPYTHON
# define LIBPYTHON_SUPPORT "yes"
#else
# define LIBPYTHON_SUPPORT "no"
#endif

#ifdef HAVE_LIBREADLINE
# define LIBREADLINE_SUPPORT "yes"
#else
# define LIBREADLINE_SUPPORT "no"
#endif

/* Supported features */
#if YYDEBUG
# define TRACE_SUPPORT "yes"
#else
# define TRACE_SUPPORT "no"
#endif

#define SUPPORTED_LIBS					\
  "Supported libraries:\n"				\
  "  libpython3 support:\t\t" LIBPYTHON_SUPPORT "\n"	\
  "  readline support:\t\t" LIBREADLINE_SUPPORT

#define SUPPORTED_FEATURES				\
  "Supported optional features:\n"			\
  /* Don't put "sorry" here since it's an easter egg */	\
  "  program tracing support:\t" TRACE_SUPPORT

/* Verbose package information */
#define PACKAGE_STRING_FULL PACKAGE_STRING " (" __DATE__ ", " __TIME__ ")\n" \
  "[" COMPILER " " __VERSION__ "]"
#define PACKAGE_STRING_EXTRA_FULL PACKAGE_STRING_FULL "\n\n"	\
  SUPPORTED_LIBS "\n\n" SUPPORTED_FEATURES

#define ENV_PREFIX		"C2PY_"
#define ENV_HISTFILE	ENV_PREFIX "HISTFILE"
#define ENV_HISTSIZE	ENV_PREFIX "HISTSIZE"

#define HISTFILE_DEFAULT ".c2py_history"

#define PYTHON_FILE "python3"

/* Default indent sizes for the prompt and output */
#define PROMPT_INDENT_SIZE	2
#define INDENT_SIZE		4
#define INDENT_SIZE_MINIFY	1

/* Long options without a short equivalent */
#define OPT_NOINTERACTIVE (CHAR_MAX + 1)

#define usage()		printf(usage_string, argv[0])
#define fusage(file)	fprintf((file), usage_string, argv[0])

#ifdef HAVE_LIBREADLINE
char editing; // Whether to enable line editing
#endif
char interactive; // Whether to run in interactive mode
unsigned int prompt_indent_size, indent_size;
char *indent_string;
char minify; // Whether to minify the output
char shebang; // Whether to run in shebang mode (pipe to python3)
char command_string_free; // Whether we need to free `command_string'
char *command_string; // String to execute when run with -c
FILE *output_file;
FILE **input_files;
char **input_file_names;
static const char usage_string[] =
  "Usage: %1$s [options] [INPUT_FILE ARG...]\n"
  "  Or:  %1$s [options] -o OUTPUT_FILE [INPUT_FILE...]\n"
  "Transpile cpy to Python.\n"
  "\n"
  "When no INPUT_FILE, or when INPUT_FILE is -, read standard input.\n"
  "When no OUTPUT_FILE (see --output), execute the code, passing ARGs in sys.argv.\n"
  "When OUTPUT_FILE is -, write to standard output\n"
  "\n"
  "  -#[PYTHON]                 run in shebang mode even if other options suggest\n"
  "                               not too. PYTHON is the name of the executable\n"
#ifdef HAVE_LIBPYTHON
  "                               that would be piped to (default "
				  PYTHON_FILE ") However,\n"
  "                               libpython support has been enabled in this\n"
  "                               version, so PYTHON is ignored\n"
#else
  "                               to be piped to (default " PYTHON_FILE ")\n"
#endif /* HAVE_LIBPYTHON */
  "  -c, --command[=COMMAND]    interpret COMMAND instead of a file\n"
#if YYDEBUG
  "  -d, --debug                trace every INPUT_FILE\n"
  "  -t, --trace                synonym for --debug\n"
#endif
#ifdef HAVE_LIBREADLINE
  "  -e, --editing              enable line editing with GNU Readline\n"
  "  -n, --noediting            do not use GNU Readline for line editing even if\n"
  "                               running interactively\n"
#endif
  "  -a, --ask                  ask before overwriting\n"
  "  -f, --force                overwrite without asking\n"
#ifdef HAVE_LIBREADLINE
  "  -p, --indent-prompt=SIZE   the size of an indent when indenting interactively\n"
  "                               (0 to disable)\n"
#endif
  "  -I, --indent-size=SIZE     the size of an indent in output\n"
  "  -i, --interactive          run interactively\n"
  "      --nointeractive        run non-interactively\n"
  "  -m, --minify[=<0|no>]      minify output; default when in shebang mode. If 0\n"
  "                               (the number zero) or \"no\" is specified, turn off\n"
  "                               minification.\n"
  "  -O, --obfuscate[=<0|no>]   alias for --minify\n"
  "  -o, --output=OUTPUT_FILE   write to OUTPUT_FILE instead of standard output\n"
  "  -!, --shebang[=PYTHON]     print a shebang line to the output file using\n"
  "                               PYTHON as the interpreter or " PYTHON_FILE " as the\n"
  "                               default. If PYTHON is not a path (does not\n"
  "                               contain a '/') and has no arguments (no spaces),\n"
  "                               the shebang line will look like this:\n"
  "                               \"#!/usr/bin/env PYTHON\". Otherwise, if PYTHON is\n"
  "                               not a path (does contain a '/') but has\n"
  "                               arguments, the shebang line will look like this:\n"
  "                               \"#!/usr/bin/env -S PYTHON\". Otherwise, if PYTHON\n"
  "                               is a path, the shebang line will look like this:\n"
  "                               \"#!PYTHON\"\n"
  "  -x, --python=PYTHON        act as if PYTHON were specified for -# or -!\n"
  "  -h, --help                 display this help and exit\n"
  "  -v, --version[=ARG]        print version information (more verbose with\n"
  "                               ARG=\"full\" and even more verbose with\n"
  "                               ARG=\"extra full\") and exit\n"
  "\n"
  "C2py webpage: " PACKAGE_URL "\n"
  "Bug report address is <" PACKAGE_BUGREPORT ">, but the prefered way to report bugs\n"
  "is at https://savannah.nongnu.org/bugs/?func=additem&group=c2py\n";

#endif /* !_C2PY_H */
