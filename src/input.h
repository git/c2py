/************************************************************************\
 * C2py converts cpy code (with a C-like syntax) to Python code.        *
 * Copyright (C) 2019  Asher Gordon <AsDaGo@posteo.net>                 *
 *                                                                      *
 * This file is part of c2py.                                           *
 *                                                                      *
 * C2py is free software: you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * C2py is distributed in the hope that it will be useful,              *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with c2py.  If not, see <https://www.gnu.org/licenses/>.       *
\************************************************************************/

/* input.h -- getc() and ungetc() frontend to get characters from
   `command_string' or interface with GNU Readline. */

/* _C2PY_INPUT_H so we don't conflict with <linux/input.h>. */
#ifndef _C2PY_INPUT_H
#define _C2PY_INPUT_H

#include "python.h"
#include "parse.h"

/* These are declared here because we use them for the prompt */
unsigned int indent_count,	/* How many times we should indent. */
  switch_count,			/* This is for naming the "switch
				   variable". See the `switch' symbol
				   in parse.y. We declare it here
				   because we need it to fix up the
				   prompt. */
  wait_string;			/* Whether we should execute the
				   string or wait for more or, for
				   get_input(), whether the prompt
				   should be ">>> " or "... ". */
/* Position information */
unsigned int line, column, column_save;
char use_ps2; // Whether to use sys.ps2 for the prompt

/* input.c uses these, so we need to define them here */

/* End a sequence of conditionals (if/for/while, elif, ..., else) */
#define end_conditional() do {				\
    if (!indent_count && wait_string) {			\
      wait_string = 0;					\
      output(""); /* Flush the buffer and run it */	\
    }							\
  } while (0)

struct indent_count {
  struct indent_count *prev;
  unsigned int count;
};

struct indent_count *do_indent_count;

#ifdef HAVE_LIBPYTHON
/* The rlcompleter function */
PyObject *rlcompleter;
#endif

int get_input(void);
int unget_input(int);
char * c2py_complete(const char *text, int state);
int indent_text(void);
#ifdef HAVE_LIBPYTHON
/* We use output("") to flush the internal `exec_str' buffer in
   output(). */
int output(const char *, ...);
#endif

#endif /* !_C2PY_INPUT_H */
